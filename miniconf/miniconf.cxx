#include "config.h"
#include <confdaemon/Options.h>
#include "Pedestal.h"
#include "Gain.h"
#include "Standalone.h"
#include "Reset.h"
#include "Reinitialize.h"
#include "EnableCards.h"
#include "Safety.h"
#include <rcuxx/Rcu.h>
#include <rcuxx/Bc.h>
#include <rcuxx/Fmd.h>
#include <rcuxx/Altro.h>
#include <rcuxx/rcu/RcuFWVERS.h>
#include <rcuxx/rcu/RcuIMEM.h>
#include <stdexcept>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <iterator>

namespace 
{
  struct to_lower 
  {
    char operator()(char c) const { return std::tolower(c); }
  };
}

void
getRange(const std::string& s, unsigned short& f, unsigned short& l)
{
  size_t dash = s.find('-'); 
  if (dash == std::string::npos) return;
  
  str2val(s.substr(0, dash), f);
  str2val(s.substr(dash+1),  l);
}



int
main(int argc, char** argv)
{
  
  Option<bool>        hOpt('h',"help",      "Show this help", false,false);
  Option<bool>        VOpt('V',"version",   "Show version number",false,false);
  Option<bool>        fOpt('f',"fmd",       "Use the FMD", true, false);
  Option<unsigned>    cOpt('c',"cards",     "Cards to turn on", 0x1);
  Option<std::string> dOpt('d',"debug",     "Debugging output");
  Option<std::string> tOpt('t',"type",      "Configuration type","STANDALONE");
  Option<std::string> lOpt('l',"log",       "Log file","");
  Option<unsigned>    GOpt('G',"int_groups","Enabled interrupt groups",0);
  Option<bool>        TOpt('T',"triggers",  "Enable triggers", false,false);
  Option<bool>        ROpt('R',"sod-rewrite","Rewrite registers on SOD",
			   false,false);
  Option<bool>        SOpt('S',"sod-event", "Enable SOD", false,false);
  Option<bool>        MOpt('M',"monitor",   "Enable monitoring", false,false);
  Option<bool>        IOpt('I',"interrupt", "Enable interrupts", false,false);
  Option<unsigned>    OOpt('O',"rate",      "Over-sampling rate", 4);
  Option<unsigned>    LOpt('L',"threshold", "Zero suppresion threshold", 2);
  Option<unsigned>    BOpt('B',"buffers",   "Number of buffers in internal "
			   "busy box", 4);
  Option<unsigned>    DOpt('D',"delay",     "Number of seconds to wait "
			   "before issuing CONFFEC", 10);
  Option<unsigned>    COpt('C',"gain-iter", "Number of iterations for gains",
			   100);
  Option<unsigned>    sOpt('s',"gain-step", "Pulser step size", 32);
  Option<std::string> rOpt('r',"strip-range", "Strips to readout","0-127");
  // Option<bool>        FOpt('3',"fmd3-workaround",
  // 			   "Enable workaround for FMD3 problem", false,false);
  Option<bool>        NOpt('N',"interleave",
			   "Enable interleaved branch readout", false,false);
  Option<bool>        WOpt('W', "no-wait",  "Do not wait for ENTER on error", 
			   false, false);
  Option<bool>        ZOpt('Z', "zero-suppress","Zero suppress", false, false);
  Option<bool>        AOpt('A', "sync-clocks","Whether to sync", false, false);
  Option<bool>        zOpt('z', "zenity","Output suitable for use with Zenity",
			   false, false);
  Option<int>         FOpt('F', "detector", "Sub-detector number", -1);
  Option<unsigned>    xOpt('x', "l1-window", "Window center for L1", 260);
  Option<unsigned>    XOpt('X', "l0-timeout", "Timeout for L0", 290); 
  Option<unsigned>    wOpt('w', "wait-delay", "Milliseconds to wait", 1000); 
  Option<unsigned>    YOpt('Y', "rcu-reset", "RCU reset flags on reinit",0); 
  CommandLine cl("");
  cl.Add(hOpt);
  cl.Add(VOpt);
  cl.Add(fOpt);
  cl.Add(dOpt);
  cl.Add(tOpt);
  cl.Add(cOpt);
  cl.Add(TOpt);
  cl.Add(ROpt);
  cl.Add(SOpt);
  cl.Add(MOpt);
  cl.Add(IOpt);
  cl.Add(WOpt);
  cl.Add(FOpt);
  cl.Add(NOpt);
  cl.Add(ZOpt);
  cl.Add(AOpt);
  cl.Add(lOpt);
  cl.Add(GOpt);
  cl.Add(OOpt);
  cl.Add(LOpt);
  cl.Add(BOpt);
  cl.Add(DOpt);
  cl.Add(zOpt);
  cl.Add(COpt);
  cl.Add(sOpt);
  cl.Add(rOpt);
  cl.Add(xOpt);
  cl.Add(XOpt);
  cl.Add(wOpt);
  cl.Add(YOpt);
  if (!cl.Process(argc, argv)) return 1;
  if (hOpt.IsSet()) {
    cl.Help();
    Rcuxx::Rcu::PrintHelp(std::cout);
    std::cout << "Configuration types can be one of\n\n"
	      << "\tSTANDALONE    Default running mode\n"
	      << "\tPEDESTAL      Pedestal calibration\n"
	      << "\tGAIN          Gain calibration mode\n"
	      << "\tPHYSICS       Data taking\n"
	      << "\tRESET	  Reset RCU and turn off FECs\n"
	      << "\tSAFETY	  Enable/disable interrupts\n"
	      << "\tREINITIALIZE  Re-initialize\n"
	      << "\tENABLE        Enable front-end cards\n"
	      << std::endl;
    return 0;
  }
  if (VOpt.IsSet()) {
    std::cout << "miniconf version " << VERSION << std::endl;
    return 0;
  }
  std::string device = (cl.Remain().size() > 0 ? 
                        cl.Remain()[0] : "echo:?fwvers=0x190606");

  int                     ret = 0;
  Rcuxx::Rcu*             rcu = Rcuxx::Rcu::Open(device.c_str(), false);
  MiniConf::Configuration* c  = 0;
  try {
    if (!rcu) throw std::runtime_error("Failed to open device");
      
    
    Rcuxx::Bc* bc = 0;
    if (fOpt.Value()) bc = new Rcuxx::Fmd(*rcu);
    else              bc = new Rcuxx::Bc(*rcu);
    Rcuxx::Altro altro(*rcu);

    std::string type(tOpt);
    std::transform(type.begin(),type.end(),type.begin(),to_lower());
    
    if (ZOpt.Value()) std::cout << "# " << std::flush;
    std::cout << "Configuring for " << type << std::endl;
    if      (type == "standalone") 
      c = new MiniConf::Standalone(*rcu, *bc, altro);
    else if (type == "pedestal")   { 
      ZOpt = false;
      c = new MiniConf::Pedestal(*rcu, *bc, altro); 
    }
    else if (type == "gain") { 
      ZOpt = false;
      c = new MiniConf::Gain(*rcu, *bc, altro); 
    }
    else if (type == "physics")    
      c = new MiniConf::Standalone(*rcu, *bc, altro);
    else if (type == "reset")    
      c = new MiniConf::Reset(*rcu, *bc, altro);
    else if (type == "safety")    
      c = new MiniConf::Safety(*rcu, *bc, altro);
    else if (type == "reinitialize")
      c = new MiniConf::Reinitialize(*rcu, *bc, altro);
    else if (type == "enable")
      c = new MiniConf::EnableCards(*rcu, *bc, altro);
    else throw std::runtime_error("Invalid run type");

    c->EnableSOD(SOpt.Value() || ROpt.Value());
    c->EnableTriggers(TOpt.Value());
    c->EnableMonitoring(MOpt.Value());
    c->EnableInterrupts(IOpt.Value());
    // c->EnableFmd3Workaround(FOpt.Value());
    c->EnableZenityOutput(zOpt.Value());
    c->EnableZeroSuppression(ZOpt.Value());
    c->EnableLogfile(lOpt.Value());
    c->EnableInterleaved(NOpt.Value());
    c->EnableRewrite(ROpt.Value());
    c->SetOversampling(OOpt.Value()); 
    c->SetZSThreshold(LOpt.Value());
    c->SetNMeb(BOpt.Value());
    c->SetInterruptGroups(GOpt.Value());
    c->SetConfigureDelay(DOpt.Value());
    c->SetNGainSamples(COpt.Value());
    c->SetGainStep(sOpt.Value());
    c->SetDetectorNumber(FOpt.Value());
    c->SetL1Window(xOpt.Value());
    c->SetL0Timeout(XOpt.Value());
    c->SetWaitDelay(wOpt.Value());
    c->SetRCUResetFlags(YOpt.Value());

    unsigned short f=0, l=127;
    getRange(rOpt.Value(), f, l);
    c->SetStripRange(f, l);
    

    if (!c->Execute(cOpt)) 
      throw std::runtime_error("Failed to execute");

    // rcu->IMEM()->Print();
  }
  catch (std::exception& e) {
    if (c) c->Error("Error: %s\n", e.what());
    else   std::cerr << e.what() << std::endl;
    std::cerr << "Failed to configure with options\n  ";
    std::copy(&(argv[1]), &(argv[argc]), 
	      std::ostream_iterator<char*>(std::cout, " "));
    ret = 1;
  }
  if (WOpt.IsSet()) {
    std::cout << "\nPress enter " << std::flush;
    char in = std::cin.get();
    (void)in;
  }
  return ret;
}

//
// EOF
//

