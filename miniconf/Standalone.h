// -*- mode: C++ -*-
#ifndef MINICONF_Standalone
#define MINICONF_Standalone
#include <miniconf/Configuration.h>


namespace MiniConf
{
  /** @brief Configure for a stand-alone run.  
      @note Currently, this is the same as the basic set-up
      done by Configuration. Eventually, we'll probably do 
      something different here */
  class Standalone : public Configuration
  {
  public:
    Standalone(Rcuxx::Rcu& rcu, Rcuxx::Bc& bc, Rcuxx::Altro& altro)
      : Configuration(rcu, bc, altro)
    {
    }
  };
}
#endif
//
// EOF
//


