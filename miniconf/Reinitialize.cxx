#include "Reinitialize.h"
#include <rcuxx/Rcu.h>
#include <rcuxx/rcu/RcuTTCControl.h>
#include <rcuxx/rcu/RcuROIConfig1.h>
#include <rcuxx/rcu/RcuROIConfig2.h>
#include <rcuxx/rcu/RcuL1Timeout.h>
#include <rcuxx/rcu/RcuL2Timeout.h>
#include <rcuxx/rcu/RcuRoiTimeout.h>
#include <rcuxx/rcu/RcuL1MsgTimeout.h>
#include <rcuxx/rcu/RcuALTROCFG1.h>
#include <rcuxx/rcu/RcuCommand.h>
#include <rcuxx/RegisterGuard.h>
#include <unistd.h>


#define PP(T,X) std::setw(50) << T << ": " << (X) << '\n' 
#define PU(T,X,U) std::setw(50) << T << ": " << (X) << U << '\n'

//____________________________________________________________________
void
MiniConf::Reinitialize::PrintSettings()
{
  std::ostream& o = std::cout;
  o << "FMD" << DetectorNumber() 
    << " [" << Timestamp() << "]: "
    << " Settings for this configuration:\n" 
    << std::boolalpha 
    << std::left
    << PP("Number of retries",                           fNRetry)
    << PP("Sync clocks",                                 fRcuResetFlags & 0x1)
    << PP("Reset RCU",                                   fRcuResetFlags & 0x2)
    << PP("Reset TTC interface",                         fRcuResetFlags & 0x4)
    << PU("Wait delay",                                  fWaitDelay, "ms")
    << PP("Detector number",                             fDetectorNumber)
    << std::noboolalpha 
    << std::right
    << std::endl;
}

//____________________________________________________________________
bool 
MiniConf::Reinitialize::ConfigureRCU()
{
  unsigned int saveRetry = fNRetry;
  fNRetry = 1;

  // Reset the RCU 
  if (!ResetRCU()) return false;

  // Now re-program BC FPGA
  if (!CommitCommand(fRcu.CONFFEC())) return false;

  // Now do N resets
  unsigned int nFECRST = 3;
  Rcuxx::RcuCommand*  rstfec  = fRcu.FECRST();
  for (unsigned int iFECRST = 0; iFECRST < nFECRST; iFECRST++) {
    Info("Reset FEC %d/%d", iFECRST+1, nFECRST);
    Wait();
    if (!CommitCommand(rstfec)) return false;
  }

  // Execute IMEM to reset registers
  if (!CommitCommand(fRcu.EXEC())) return false;

  // We are now done
  return true;

}

//____________________________________________________________________
//
// EOF
//
