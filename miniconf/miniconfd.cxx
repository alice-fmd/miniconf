#include "config.h"
#include <confdaemon/Options.h>
#include "Daemon.h"
#ifndef PACKAGE_URL
# define PACKAGE_URL "http://fmd.nbi.dk/fmd/fee/software.html#miniconf"
#endif

int
main(int argc, char** argv)
{
  
  Option<bool>        hOpt('h', "help",     "Show this help", false,false);
  Option<bool>        VOpt('V', "version",  "Show version number",false,false);
  Option<bool>        fOpt('f', "fmd",      "Use the FMD", false,false);
  Option<bool>        rOpt('r', "retry",    "Retry command", false,false);
  Option<bool>        aOpt('a', "enable-done","Enable done state",false,false);
  Option<bool>        ROpt('R', "enable-recover","Enable recovery state", 
			   false,false);
  Option<std::string> nOpt('n', "base",     "Base name", "MINICONF");
  Option<std::string> DOpt('D', "dns",      "DIM DNS node", "alifmddimdns");
  Option<std::string> NOpt('N', "name",     "Name of FEE", "FMD-FEE_0_0_0");
  Option<std::string> tOpt('t', "url",      "URL to connect to", "");
  Option<std::string> pOpt('p', "pid",      "Pid file");
  Option<std::string> iOpt('i', "input",    "Ignored (compatiblity)");
  Option<std::string> BOpt('B', "bindir",   "Directory holding miniconf", 
			   BINDIR);
  Option<unsigned>    TOpt('T', "timeout",  "Time out in seconds",   120);
  Option<unsigned>    WOpt('W', "delay",    "Delay before executing",  10);
  Option<bool>        AOpt('A', "enable-arm","Enable ARM_ASYNCH on reinit", 
			   false,false);
  Option<unsigned>    SOpt('S',"rcu-reset",  "Flags for RCU reset"
			   "(0x1:ARM_ASYNC,0x2:RCU_RESET:0x4:TTC_RESET)", 0);
  
  CommandLine cl(""); 
  cl.Add(hOpt);
  cl.Add(VOpt);
  cl.Add(fOpt);
  cl.Add(rOpt);
  cl.Add(NOpt);
  cl.Add(tOpt);
  cl.Add(nOpt);
  cl.Add(DOpt);
  cl.Add(pOpt);
  cl.Add(BOpt);
  cl.Add(TOpt);
  cl.Add(WOpt);
  cl.Add(aOpt);
  cl.Add(iOpt);
  cl.Add(ROpt);
  cl.Add(SOpt);
  if (!cl.Process(argc, argv)) return 1;
  if (hOpt.IsSet()) {
    cl.Help();
    return 0;
  }
  if (VOpt.IsSet()) {
    std::cout << PACKAGE_STRING << std::endl;
    return 0;
  }

  std::string pfx = nOpt;
  std::string dns = DOpt;
  std::string nme = NOpt;
  std::string pid = pOpt;
  std::string bin = BOpt;
  std::string tgt = tOpt;
  bool        rty = rOpt;
  bool        fmd = fOpt;
  unsigned    tmo = TOpt;
  unsigned    dly = WOpt;
  bool        dne = aOpt;
  bool        rcy = ROpt;
  unsigned    rcu = SOpt;


  MiniConf::Daemon daemon(pfx, dns, nme, tgt, bin, pid, tmo, dly, rty, fmd);
  daemon.UseDone(dne);
  daemon.UseRecovery(rcy);
  daemon.SetRCUResetFlags(rcu);
  daemon.Print(std::cout);
  std::cout << "\tPID file:      " << pid << std::endl;

  return daemon.Run();
}

//
// EOF
//

  
