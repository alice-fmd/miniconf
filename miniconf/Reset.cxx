#include "Reset.h"
#include <rcuxx/Rcu.h>
#include <rcuxx/rcu/RcuACTFEC.h>
#include <rcuxx/rcu/RcuCommand.h>
#include <rcuxx/rcu/RcuACL.h>
#include <rcuxx/rcu/RcuHITLIST.h>
#include <rcuxx/rcu/RcuTRGCONF.h>
#include <unistd.h>

//____________________________________________________________________
bool 
MiniConf::Reset::TurnOnCards(unsigned int)
{
  Rcuxx::RcuACTFEC*   actfec   = fRcu.ACTFEC();
  Rcuxx::RcuACL*      acl      = fRcu.ACL();
  Rcuxx::RcuHITLIST*  hitlist  = fRcu.HITLIST();
  actfec->SetValue(0);
  if (!CommitRegister(actfec, true)) return false;
  sleep(1);
  for (unsigned int i = 0; i < 32; i++) { 
    acl->DisableBoard(i);
  }
  unsigned int ret = acl->Commit();
  if (ret) {
    Error("Failed to write ACL: %s", fRcu.ErrorString(ret).c_str());
    return false;
  }
  if (!hitlist) return true;
  Info("Clearing HITLIST - ignore warnings from FeeServer");
  unsigned int val = 0xffffffff;
  for (size_t i = 0; i < hitlist->Size(); i++) 
    hitlist->Set(i, 1, &val);
  ret = hitlist->Commit();
  if (ret) {
    Error("Failed to write HITLIST: %s", fRcu.ErrorString(ret).c_str());
    return false;
  }

  return true;
}

//____________________________________________________________________
bool 
MiniConf::Reset::ConfigureRCU()
{
  Rcuxx::RcuCommand*  glb_reset = fRcu.GLB_RESET();
  Rcuxx::RcuCommand*  ttc_reset = fRcu.TTCReset();
  if (!CommitCommand(glb_reset)) return false;
  sleep(1);
  if (!CommitCommand(glb_reset)) return false;
  sleep(1);
  if (!CommitCommand(ttc_reset)) return false;
  sleep(1);
  Rcuxx::RcuTRGCONF* trgconf = fRcu.TRGCONF();
  if (!trgconf) { 
    Error("Couldn't find either L1_TTC nor TRGCONF");
    return true;
  }
  trgconf->Update();
  trgconf->EnableTTCTriggers(false);
  return CommitRegister(trgconf, true);
}

//____________________________________________________________________
//
// EOF
//
