// -*- mode: C++ -*-
#ifndef MINICONF_Safety
#define MINICONF_Safety
#include <miniconf/Configuration.h>


namespace MiniConf
{
  /** @brief Configure for a finish run.  
      @note Currently, this is the same as the basic set-up
      done by Configuration. Eventually, we'll probably do 
      something different here */
  class Safety : public Configuration
  {
  public:
    Safety(Rcuxx::Rcu& rcu, Rcuxx::Bc& bc, Rcuxx::Altro& altro)
      : Configuration(rcu, bc, altro), _on(true), fEnableTriggers(true)
    {
    }
    bool TurnOnCards(unsigned int mask);
    bool ConfigureRCU();
    bool ConfigureALTROs();
    bool ConfigureBCs();
    bool ConfigureALTRO(unsigned int board, unsigned int chip);
    // bool ConfigureBC(unsigned int board);
    bool ReadConfigurationALTRO(unsigned int board,unsigned int chip);
    bool ReadConfigurationBC(unsigned int board);
    bool TurnOnMonitoring() { return true; }
    bool TurnOnInterrupts() { return true; }
    bool TurnOnTriggers();
    bool SendConfigured()   { return true; }
    bool _on;
    bool fEnableTriggers;
  };
  inline bool Safety::ConfigureRCU() { return true; }
  inline bool Safety::ConfigureALTROs() { return true; }
  inline bool Safety::ConfigureBCs() { return true; }
  inline bool Safety::ConfigureALTRO(unsigned int, unsigned int) {return true;}
  // inline bool Safety::ConfigureBC(unsigned) { return true; }
  inline bool Safety::ReadConfigurationBC(unsigned) { return true; }
  inline bool Safety::ReadConfigurationALTRO(unsigned int,unsigned int)
  {return true;}

}
#endif
//
// EOF
//


