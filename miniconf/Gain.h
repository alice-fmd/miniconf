// -*- mode: C++ -*-
#ifndef MINICONF_Gain
#define MINICONF_Gain
#include <miniconf/Configuration.h>


namespace MiniConf
{
  class Gain : public Configuration
  {
  public:
    Gain(Rcuxx::Rcu& rcu, Rcuxx::Bc& bc, Rcuxx::Altro& altro)
      : Configuration(rcu, bc, altro)
    {
      fZeroSuppress = false;
    }
    void EnableZeroSuppression(bool) { }
  protected:
    bool ConfigureBCs();
    bool ConfigureALTROs();
    bool ReadConfigurationBC(unsigned int id);
    unsigned int CalcSamples() const;
  };
}
#endif
//
// EOF
//


