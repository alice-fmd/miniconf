// -*- mode: C++ -*-
#ifndef MINICONF_Reinitialize
#define MINICONF_Reinitialize
#include <miniconf/Configuration.h>


namespace MiniConf
{
  /** 
   * @brief Reinitialize for a run.  
   * 
   * @note This flashes the BC FPGA with configuration from flash, and
   * then resets the registers using the prepared IMEM.  
   */
  class Reinitialize : public Configuration
  {
  public:
    /** 
     * Constructor 
     * 
     * @param rcu   Pointer to RCU 
     * @param bc    Pointer to BC
     * @param altro Pointer to ALTRO 
     */
    Reinitialize(Rcuxx::Rcu& rcu, Rcuxx::Bc& bc, Rcuxx::Altro& altro)
      : Configuration(rcu, bc, altro)
    {
    }
    /** 
     * The only function here that does anything 
     * 
     * @return true on success, false otherwise 
     */
    bool ConfigureRCU();
    /** 
     * Does nothing 
     * 
     * @return always true
     */
    bool TurnOnCards(unsigned int) { return true; }
    /** 
     * Does nothing 
     * 
     * @return always true
     */
    bool ConfigureALTROs()  { return true; }
    /** 
     * Does nothing 
     * 
     * @return always true
     */
    bool ConfigureBCs()  { return true; }
    /** 
     * Does nothing 
     * 
     * @return always true
     */
    bool ConfigureALTRO(unsigned int, unsigned int) { return true; }
    /** 
     * Does nothing 
     * 
     * @return always true
     */
    bool ConfigureBC(unsigned int) { return true; }
    /** 
     * Does nothing 
     * 
     * @return always true
     */
    bool ReadConfigurationALTRO(unsigned int,unsigned int) { return true; }
    /** 
     * Does nothing 
     * 
     * @return always true
     */
    bool ReadConfigurationBC(unsigned int) { return true; }
    /** 
     * Does nothing 
     * 
     * @return always true
     */
    bool TurnOnMonitoring() { return true; }
    /** 
     * Does nothing 
     * 
     * @return always true
     */
    bool TurnOnInterrupts() { return true; }
    /** 
     * Does nothing 
     * 
     * @return always true
     */
    bool TurnOnTriggers()   { return true; }
    /** 
     * Does nothing 
     * 
     * @return always true
     */
    bool SendConfigured()   { return true; }

    void PrintSettings();
  };

}
#endif
//
// EOF
//


