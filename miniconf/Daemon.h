// -*- mode: C++ -*-
#ifndef MINICONF_Daemon
#define MINICONF_Daemon
#include <confdaemon/DaemonUtils.h>
#include <sstream>

namespace MiniConf
{
  /** 
   * @brief Daemon running in the background and spawns miniconf
   * application on demand.  
   */
  class Daemon : public DaemonUtils::Base
  {
  public:
    /** 
     * Constructor 
     *
     * @param base    Base name of daemon (server)
     * @param dns     DNS node to use 
     * @param name    Name of FEE
     * @param target  Target interface 
     * @param bindir  Where the program is
     * @param pidfile File to append PID to 
     * @param timeout Timeout in seconds 
     * @param delay   Delay before executing child program
     * @param retry   Whether to retry
     * @param fmd     Use FMD 
     */
    Daemon(const std::string& base, 
	   const std::string& dns, 
	   const std::string& name,
	   const std::string& target,
	   const std::string& bindir,
	   const std::string& pidfile,
	   unsigned int       timeout,
	   unsigned int       delay,
	   bool               retry=true,
	   bool               fmd=true);
    /** 
     * Destructor 
     */
    ~Daemon() {}
    void Print(std::ostream& o) const;
    void SetRCUResetFlags(unsigned int flags) { fRcuResetFlags = flags; }
  protected:
    /** Do nothing */
    void SetupAuxServices() {}
    bool SetOption(const char*        what, 
		   const char*        opt, 
		   const std::string& val, 
		   std::vector<std::string>& args) const;
    /** 
     * Make the command line 
     *
     * @param args
     */ 
    void MakeCommandLine(const std::string& str, 
			 std::vector<std::string>& args);
	
    /** Whether to use FMD */ 
    bool fUseFMD;
    /** Detector number */ 
    int fDetectorNumber;
    /** Flags for RCU reset */
    unsigned int fRcuResetFlags;
  };
}
#endif
//
// EOF
//


