#include "Safety.h"
#include <rcuxx/Rcu.h>
#include <rcuxx/rcu/RcuACTFEC.h>
#include <rcuxx/rcu/RcuCommand.h>
#include <rcuxx/rcu/RcuACL.h>
#include <rcuxx/rcu/RcuHITLIST.h>
#include <rcuxx/rcu/RcuTRGCONF.h>
#include <rcuxx/Bc.h>
#include <rcuxx/bc/BcCSR0.h>
#include <unistd.h>

//____________________________________________________________________
bool 
MiniConf::Safety::TurnOnCards(unsigned int /* mask */) 
{ 
  Rcuxx::RcuACTFEC* actfec  = fRcu.ACTFEC();
  unsigned int ret = actfec->Update();
  if (ret) { 
    Error("Failed to read %s: %s", actfec->Name().c_str(), 
	  fRcu.ErrorString(ret).c_str());
    return false;
  }
  return true; 
}


//____________________________________________________________________
bool 
MiniConf::Safety::TurnOnTriggers()
{
  Info("*** Turning %s triggers\n", fEnableTriggers ? "on" : "off");
  Rcuxx::RcuCommand* l1ttc = fRcu.L1_TTC();
  Rcuxx::RcuCommand* l1cmd = fRcu.L1_CMD();
  if (l1ttc && fEnableTriggers)  return CommitCommand(l1ttc);
  if (l1cmd && !fEnableTriggers) return CommitCommand(l1cmd);

  Rcuxx::RcuTRGCONF* trgconf = fRcu.TRGCONF();
  if (!trgconf) { 
    Error("Couldn't find either L1_TTC nor TRGCONF");
    return false;
  }
  trgconf->Update();
  trgconf->EnableTTCTriggers(fEnableTriggers);
  
  return CommitRegister(trgconf);
}

//____________________________________________________________________
//
// EOF
//
