// -*- mode: C++ -*-
#ifndef MINICONF_Configuration
#define MINICONF_Configuration
#include <iosfwd>
#include <string>
#include <vector>

// Forward declaration
namespace Rcuxx
{
  class Rcu;
  class Bc;
  class Altro;
  class Register;
  class Command;
  class AltroRegister;
  class AltroCommand;
}
  

/** @defgroup base
    @brief Mini configuration of FMD FEE */
/** @namespace MiniConf 
    @ingroup base
    @brief Namespace for all classes */
namespace MiniConf
{
  /** Base class for configurations 
      @ingroup base */
  class Configuration 
  {
  public:
    enum { 
      kIntT  = 0x01, 
      kIntAc = 0x02,  
      kIntAv = 0x04, 
      kIntDc = 0x08, 
      kIntDv = 0x10
    };
    /** Destructor */
    virtual ~Configuration();
    /** Execute the configurator 
	@param cards Bit mask of cards to turn on */
    bool Execute(unsigned int cards=0x30003);

    /** @{
	@name Various settings */
    /** Whether to re/write on SOD - implies EnableSOD */
    void EnableRewrite(bool use=true) { fEnableRewrite = use; }
    /** Whether to turn SOD read of configuration */ 
    void EnableSOD(bool use=true) { fEnableSOD = use; }
    /** Whether to turn monitorings */ 
    void EnableMonitoring(bool use=true) { fEnableMonitoring = use; }
    /** Whether to turn interrupts */ 
    void EnableInterrupts(bool use=true) { fEnableInterrupts = use; }
    /** Whether to turn triggers */ 
    void EnableTriggers(bool use=true) { fEnableTriggers = use; }
    /** Whether to enable full set */
    void EnableFmd3Workaround(bool use=true) { fFmd3Workaround = use; }
    /** Whether to enable full set */
    void EnableZenityOutput(bool use=true) { fZenityOutput = use; }
    /** Whether to enable interleaved read-out */
    void EnableInterleaved(bool use=true) { fInterleaved = use; }
    /** Whether to enable a log file */
    void EnableLogfile(const std::string& logfile);
    /** Whether to enable zero suppression */ 
    virtual void EnableZeroSuppression(bool use=true) { fZeroSuppress = use; }
    /** 
     * Set oversampling. This should be 4 if zero suppression is
     * enabled. 
     * 
     * @param r  Ratio of the shift clock (VA1 shift out) and the
     *        sample clock (ADC clock in ALTRO).   
     */    
    void SetOversampling(unsigned int r);
    /** 
     * Set the threshold used for zero suppression. This number must
     * not be 2, or the zero-suppression will not work.  If it is set
     * to zero it means no zero-suppression.
     * 
     * @param thr Threshold (in ADC counts) of the zero suppression
     *        filter.
     */
    void SetZSThreshold(unsigned short thr=2) { fZSThreshold = thr; }
    /** 
     * Set the number of multi-event buffers to use in the internal
     * busy-box.  If set to zero the internal busy box is disabled.
     * The value should not exceed 4. 
     * 
     * @param n Number of buffers used in the internal busy box.
     */
    void SetNMeb(unsigned short n=4) { fNMeb = n; }
    /** 
     * Set the bit mask of interrupts to enable on the BCs
     * 
     * @param m Bit mask of enabled interrupts
     */
    void SetInterruptGroups(unsigned short m=0) { fIntGroups = m; }
    /** 
     * Set the configuration delay.  The time to wait before issuing
     * CONFFEC after setting ACTFEC. 
     * 
     * @param d Number of seconds between ACTFEC and CONFFEC
     */
    void SetConfigureDelay(unsigned short d=10) { fConfDelay = d; }
    /** 
     * Set the number of events to read for each gain step for each
     * strip 
     * 
     * @param n Number of events 
     */
    void SetNGainSamples(unsigned short n=100) { fNGainSamples = n; }
    /** 
     * Set the step size for the gain pulser. 
     * 
     * @param s Step size (0-255)
     */
    void SetGainStep(unsigned short s=32) { fGainStep = s; }
    /** 
     * Set the detector number 
     *
     * @param d Detector number - if <0 auto-deduce if possible 
     */
    void SetDetectorNumber(int d) { fDetectorNumber = d; }
    /** 
     * Set the center of the L1 window (on the RCU) 
     *
     * @param cs Center of the L1 window in 40MHz clock cycles
     */
    void SetL1Window(unsigned short cs) { fL1Window = cs; }
    /**
     * Set the L0 timeout (on the BCs) 
     *
     * @param cs Timeout in 40MHz clock cycles  
     */
    void SetL0Timeout(unsigned short cs) { fL0Timeout = cs; }
    /** 
     * Set the strip range.  
     *
     * @note This should not be used except for very special
     * purposes. 
     * 
     * @param min First strip (0-127) 
     * @param max Last strip (0-127)
     */
    void SetStripRange(unsigned short min=0, unsigned short max=127)
    {
      fMinStrip = min;
      fMaxStrip = max;
    }
    /** 
     * Set the time to sleep between commands that need and explicit wait 
     * 
     * @param ms Time (in miliseconds) to sleep 
     */
    void SetWaitDelay(unsigned int ms) { fWaitDelay = ms; }
    /** 
     * What to do on RCU configuration when reinitializing 
     *
     * @param flags Flags 
     *
     * - 0x1: ARM_ASYNC 
     * - 0x2: RCU_RESET 
     * - 0x4: TTC_RESET 
     */
    void SetRCUResetFlags(unsigned int flags) { fRcuResetFlags = flags; }
    /** @} */

    /** @{ 
	@name Logging and messages */
    /** Report an error */ 
    void Error(const char* msg, ...);
    /** Report a warning */ 
    void Warn(const char* msg, ...);
    /** Report information */ 
    void Info(const char* msg, ...);
    /** @} */

    virtual void PrintSettings();
  protected:
    /** @{
	@name basic interface */
    /** Constructor 
	@param rcu Reference to RCU interface */
    Configuration(Rcuxx::Rcu&   rcu, 
		  Rcuxx::Bc&    bc, 
		  Rcuxx::Altro& altro) 
      : fRcu(rcu), 
	fBc(bc), 
	fAltro(altro), 
	fEnableSOD(true),
	fEnableRewrite(true),
	fEnableTriggers(true),
	fEnableMonitoring(true),
	fEnableInterrupts(true),
	fInterleaved(true),
	fZeroSuppress(true),
	fZSThreshold(0),
	fNRetry(3),
	fFmd3Workaround(false),
	fZenityOutput(false),
	fLog(0),
	fRatio(2),
	fNMeb(1),
	fConfDelay(10),
	fWaitDelay(1000),
	fNGainSamples(100),
	fGainStep(32),
	fMinStrip(0), 
	fMaxStrip(127),
	fRcuResetFlags(0),
	fDetectorNumber(-1), 
	fL1Window(260), 
	fL0Timeout(270),
	fImemCache(0), 
	fImemMarker(0)
    {
    }
    /** Copy constructor 
	@param other Object to copy from */ 
    Configuration(const Configuration& other) 
      : fRcu(other.fRcu), 
	fBc(other.fBc), 
	fAltro(other.fAltro), 
	fEnableSOD(other.fEnableSOD),
	fEnableRewrite(other.fEnableRewrite),
	fEnableTriggers(other.fEnableTriggers),
	fEnableMonitoring(other.fEnableMonitoring),
	fEnableInterrupts(other.fEnableInterrupts),
	fInterleaved(other.fInterleaved),
	fZeroSuppress(other.fZeroSuppress),
	fRatio(other.fRatio),
	fNMeb(other.fNMeb),
	fConfDelay(other.fConfDelay), 
	fWaitDelay(other.fWaitDelay),
	fNGainSamples(other.fNGainSamples),
	fGainStep(other.fGainStep),
	fMinStrip(other.fMinStrip),
	fMaxStrip(other.fMaxStrip),
	fRcuResetFlags(other.fRcuResetFlags),
	fDetectorNumber(other.fDetectorNumber),
	fL1Window(other.fL1Window),
	fL0Timeout(other.fL0Timeout),
	fImemCache(other.fImemCache),
	fImemMarker(other.fImemMarker)
    {}
    /** @} */

    /** @{ 
	@name Specific functions */ 
    /** Turn on and reset front-end cards */ 
    virtual bool TurnOnCards(unsigned int mask);
    /** Reset the RCU */
    virtual bool ResetRCU();
    /** Configure RCU */
    virtual bool ConfigureRCU();
    /** Configure ALTROs (in broadcast) */
    virtual bool ConfigureALTROs();
    /** Configure BCs (in broadcast) */
    virtual bool ConfigureBCs();
    /** Configure one ALTRO
	@param address Address of ALTRO */
    virtual bool ConfigureALTRO(unsigned int board, unsigned int chip);
    /** Configure one BC (in broadcast)
	@param address Address of BC */
    virtual bool ConfigureBC(unsigned int board);
    /** Read the configurations into SOD event */
    virtual bool ReadConfiguration();
    /** Read the configurations into SOD event */
    virtual bool ReadConfigurationBC();
    /** Read the configurations into SOD event */
    virtual bool ReadConfigurationALTRO();
    /** Read configuration of BC into RMEM (and event) on SOD event 
	@param board Board address */
    virtual bool ReadConfigurationBC(unsigned int board);
    /** Read configuration of ALTRO into RMEM (and event) on SOD event 
	@param board Board address 
	@param chip  Chip address*/
    virtual bool ReadConfigurationALTRO(unsigned int board, unsigned int chip);
    /** Enable monitoring */ 
    virtual bool TurnOnMonitoring();
    /** Enable interrupts */ 
    virtual bool TurnOnInterrupts();
    /** Enable interrupts */ 
    virtual bool TurnOnTriggers();
    /** Send configured command to RCU */
    virtual bool SendConfigured();
    /** @} */

    /** @{ 
	@name Utility functions */
    /** Check with the RCU if a FEC is on */ 
    bool IsBCOn(unsigned int board);
    /** Check with the RCU if a ALTRO is on */ 
    bool IsALTROOn(unsigned int board, unsigned int chip);
    /** Commit a register */ 
    bool DoCommitRegister(Rcuxx::Register* reg, bool verify=false);
    /** Commit a register */ 
    bool CommitRegister(Rcuxx::Register* reg, bool verify=false) { return DoCommitRegister(reg,verify); }
    /** Commit a register */ 
    bool CommitRegister(Rcuxx::AltroRegister* reg, bool verify=false);
    /** Verify register content */
    bool DoVerifyRegister(Rcuxx::Register* reg);
    /** Commit a register */ 
    bool DoCommitCommand(Rcuxx::Command* cmd);
    /** Commit a register */ 
    bool CommitCommand(Rcuxx::Command* cmd) { return DoCommitCommand(cmd); }
    /** Commit a register */ 
    bool CommitCommand(Rcuxx::AltroCommand* cmd);
    /** Read register on SOD */
    bool ReadRegister(Rcuxx::AltroRegister* reg);
    /** Read command on SOD */
    bool ReadCommand(Rcuxx::AltroCommand* reg);
    /** Get the detector number */
    int DetectorNumber() const;
    /** @} */
    /** 
     * Calculate the number of samples to do 
     * 
     * @return Number of samples to do 
     */
    virtual unsigned int CalcSamples() const;
    /** 
     * Return a string with current time 
     * 
     * @return Current time 
     */
    std::string Timestamp() const;
    /** 
     * Do a sleep of specified time (in miliseconds) 
     * 
     * @param sleep How long to sleep in miliseconds.  If negative use
     * value of fWaitDelay.
     */
    virtual void Wait(int sleep=-1);

    /** Type of IMEM cache */ 
    typedef std::vector<unsigned int> IMEMCache_t;

    /** Reference to RCU interface */
    Rcuxx::Rcu& fRcu;
    /** Reference to BC interface */
    Rcuxx::Bc& fBc;
    /** Reference to ALTRO interface */
    Rcuxx::Altro& fAltro;
    /** Whether to enable SOD event */
    bool fEnableSOD;
    /** Whether to re/write on SOD */
    bool fEnableRewrite;
    /** Whether to enable triggers */
    bool fEnableTriggers;
    /** Whether to enable monitoring */
    bool fEnableMonitoring;
    /** Whether to enable interrupts */
    bool fEnableInterrupts;
    /** Whether to enable interleaved branch read-out */
    bool fInterleaved;
    /** Whether to enable zero suppression */
    bool fZeroSuppress;
    /** Zero suppression threshold in ADC counts */ 
    unsigned short fZSThreshold;
    /** Number of retries */
    unsigned int fNRetry;
    /** Work around FMD3 bug */
    bool fFmd3Workaround;
    /** Output suitable for Zenity */
    bool fZenityOutput;
    /** Optional log file */
    std::ostream* fLog;
    /** Oversampling rate */
    unsigned int fRatio;
    /** Number of buffers */
    unsigned short fNMeb;
    /** Interrupt groups */
    unsigned short fIntGroups;
    /** The delay (in seconds) between ACTFEC and CONFFEC */
    unsigned short fConfDelay;
    /** Wait delay in miliseconds */
    unsigned int fWaitDelay;
    /** number of samples per pulser value per strip */
    unsigned short fNGainSamples;
    /** Step size in gain pulser */
    unsigned short fGainStep;
    /** First strip to read out */
    unsigned short fMinStrip;
    /** Last strip to read out */
    unsigned short fMaxStrip;
    /** RCU reset flags */
    unsigned short fRcuResetFlags;
    /** Detector number */
    int fDetectorNumber;
    /** L1 window center - RCU setting */
    unsigned short fL1Window;
    /** L0 timeout - BC setting */
    unsigned short fL0Timeout;
    /** Instruction memory cache */ 
    IMEMCache_t fImemCache;
    /** Current pointer into IMEM cache */ 
    size_t fImemMarker;
  };
}

#endif
//
// EOF
//

  
