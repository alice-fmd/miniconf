#include "Daemon.h"
#include "config.h"
#include <cstdlib>
#include <stdexcept>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <unistd.h>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <iterator>
#include <cstdarg>
#ifndef CONF_PROGRAM
# define CONF_PROGRAM "miniconf"
#endif
#ifndef PACKAGE_URL
# define PACKAGE_URL "http://fmd.nbi.dk/fmd/fee/software.html"
#endif

//____________________________________________________________________
namespace 
{
  struct to_lower 
  {
    char operator()(char c) const { return std::tolower(c); }
  };
}

//____________________________________________________________________
MiniConf::Daemon::Daemon(const std::string& base, 
			 const std::string& dns,
			 const std::string& name,
			 const std::string& url, 
			 const std::string& bindir,
			 const std::string& pidfile,
			 unsigned int       timeout,
			 unsigned int       delay,
			 bool               retry,
			 bool               fmd)
  : DaemonUtils::Base(base, dns, url, name, bindir, pidfile, 
		      timeout, delay, retry),
    fUseFMD(fmd),
    fDetectorNumber(-1),
    fRcuResetFlags(0)
{
  char l = name[name.size()-1];
  switch (l) { 
  case '1': fDetectorNumber = 1; break;
  case '2': fDetectorNumber = 2; break;
  case '3': fDetectorNumber = 3; break;
  }
}

//____________________________________________________________________
bool
MiniConf::Daemon::SetOption(const char*               what, 
			    const char*               opt,
			    const std::string&        str, 
			    std::vector<std::string>& args) const
{
  if  (str.find(what) == std::string::npos) return false;
  
  size_t eq = str.find("=");
  if (eq == std::string::npos) {
    args.push_back(opt);
    return true;
  }
  
  std::string s(str.substr(eq+1, str.size()-eq-1));
 
  args.push_back(opt);
  args.push_back(s);

  return true;
}

//____________________________________________________________________
void 
MiniConf::Daemon::MakeCommandLine(const std::string& str, 
				  std::vector<std::string>& args)
{
  std::string              tag;
  bool                     zs          = false;
  unsigned int             cards       = 0;
  unsigned short           thr         = 0;
  std::string              enab_ints   = "";
  std::string              range       = "";
  std::stringstream 	   s(str);
  
  // Image 
  std::stringstream scmd;
  scmd << fBinDir << "/" << CONF_PROGRAM;
  args.push_back(scmd.str());

  // Push detector number as option to sub-process
  std::stringstream darg; 
  darg << fDetectorNumber;
  args.push_back("-F");
  args.push_back(darg.str());
  
  // Target 
  // std::stringstream tcmd;
  // tcmd << "fee://" << fDns << "/" << fTarget;
  // tcmd << "echo:?fwvers=0xffffff";
  // args.push_back(tcmd.str());
  args.push_back(fUrl);
  
  // Tag 
  GetPart("tag", s, tag);
  args.push_back("-t");
  args.push_back(tag);
  
  // Cards 
  GetPart("cards", s, cards);
  std::stringstream ccmd;
  ccmd << "0x" << std::hex << cards;
  args.push_back("-c");
  args.push_back(ccmd.str());
  
  while (!s.eof()) { 
    std::string opt;
    std::string val;
    GetPart("option", s, opt);
    std::transform(opt.begin(),opt.end(),opt.begin(),to_lower());
    
    if       (SetOption("monitor",    "-M", opt, args));
    else if  (SetOption("interrupt",  "-I", opt, args));
    else if  (SetOption("sod",        "-S", opt, args));
    else if  (SetOption("trigger",    "-T", opt, args));
    else if  (SetOption("interleaved","-N", opt, args));
    else if  (SetOption("fmd3",       "-3", opt, args));
    // else if  (opt == "four")              over        = 4;
    // else if  (opt == "two")               over        = 2;
    // else if  (opt == "one")               over        = 1;
    else if  (SetOption("nbuf",       "-B", opt, args));
    else if  (SetOption("over",       "-O", opt, args));
    else if  (SetOption("conf_delay", "-D", opt, args));
    else if  (SetOption("cal_iter",   "-C", opt, args));
    else if  (SetOption("cal_step",   "-s", opt, args));
    else if  (SetOption("range",      "-r", opt, args));
    else if  (SetOption("rewrite",    "-R", opt, args));
    else if  (SetOption("l1window",   "-x", opt, args));
    else if  (SetOption("l0timeout",  "-X", opt, args));
    else if  (SetOption("wait",       "-w", opt, args));
    // else if  (SetOption("sync",       "-A", opt, args));
    else if  (GetValue("zero",       opt, thr)) zs    = thr > 0;
    else if  (GetValue("int_groups", opt, enab_ints));
    else if  (!opt.empty())
      Message(kMsgWarning, "Unknown option: %s\n", opt.c_str());
  }
  

  // Enabled interrupt groups 
  if (!enab_ints.empty()) { 
    int mask = 0;
    std::transform(enab_ints.begin(),enab_ints.end(),enab_ints.begin(),
		   to_lower());
    std::stringstream is(enab_ints);
    while (true) { 
      std::string s;
      std::getline(is, s, '|');
      if (s.empty()) break;

      if      (s == "t")   mask |= 0x01; // MiniConf::Configuration::kIntT;
      else if (s == "ac")  mask |= 0x02; // MiniConf::Configuration::kIntAc;
      else if (s == "av")  mask |= 0x04; // MiniConf::Configuration::kIntAc;
      else if (s == "dc")  mask |= 0x08; // MiniConf::Configuration::kIntDc;
      else if (s == "dv")  mask |= 0x10; // MiniConf::Configuration::kIntDv;
    }

    std::stringstream gcmd;
    gcmd << mask;
    args.push_back("-G");
    args.push_back(gcmd.str());
  }
  // Other options
  if (fUseFMD)        args.push_back("-f");
  if (zs) {
    std::stringstream lcmd;
    lcmd << thr;
    args.push_back("-Z");
    args.push_back("-L");
    args.push_back(lcmd.str());
  }
  // Rcu reset 
  std::stringstream rcmd;
  rcmd << "0x" << std::hex << fRcuResetFlags;
  args.push_back("-Y");
  args.push_back(rcmd.str());
}

//____________________________________________________________________
void
MiniConf::Daemon::Print(std::ostream& o) const
{
  o << PACKAGE_STRING    << "\n"
    << PACKAGE_URL       << "\n"
    << PACKAGE_BUGREPORT << std::endl;
  DaemonUtils::Base::Print(o);
  o << std::boolalpha
    << "\tUse FMD:            " << fUseFMD << '\n'
    << "\tDetector number:    " << fDetectorNumber << '\n'
    << "\tUse ARM_ASYNC:      " << (fRcuResetFlags & 0x1) << '\n'
    << "\tUse RCU_RESET:      " << (fRcuResetFlags & 0x2) << '\n'
    << "\tUse TTC_RESET:      " << (fRcuResetFlags & 0x4)
    << std::noboolalpha << std::endl;
}

//____________________________________________________________________
//
// EOF
//
