// -*- mode: C++ -*-
#ifndef MINICONF_EnableCards
#define MINICONF_EnableCards
#include <miniconf/Configuration.h>


namespace MiniConf
{
  /** 
   * @brief EnableCards for a run.  
   * 
   * @note This simple enables cards in the ACTFEC register.  Needed
   * to avoid trips on some power supplies.
   */
  class EnableCards : public Configuration
  {
  public:
    /** 
     * Constructor 
     * 
     * @param rcu   Pointer to RCU 
     * @param bc    Pointer to BC
     * @param altro Pointer to ALTRO 
     */
    EnableCards(Rcuxx::Rcu& rcu, Rcuxx::Bc& bc, Rcuxx::Altro& altro)
      : Configuration(rcu, bc, altro)
    {
    }
    /** 
     * Does nothing 
     * 
     * @return Always true
     */
    bool ConfigureRCU() { return true; }
    /** 
     * The only function here that does anything 
     * 
     * @return always true
     */
    bool TurnOnCards(unsigned int);
    /** 
     * Does nothing 
     * 
     * @return always true
     */
    bool ConfigureALTROs()  { return true; }
    /** 
     * Does nothing 
     * 
     * @return always true
     */
    bool ConfigureBCs()  { return true; }
    /** 
     * Does nothing 
     * 
     * @return always true
     */
    bool ConfigureALTRO(unsigned int, unsigned int) { return true; }
    /** 
     * Does nothing 
     * 
     * @return always true
     */
    bool ConfigureBC(unsigned int) { return true; }
    /** 
     * Does nothing 
     * 
     * @return always true
     */
    bool ReadConfigurationALTRO(unsigned int,unsigned int) { return true; }
    /** 
     * Does nothing 
     * 
     * @return always true
     */
    bool ReadConfigurationBC(unsigned int) { return true; }
    /** 
     * Does nothing 
     * 
     * @return always true
     */
    bool TurnOnMonitoring() { return true; }
    /** 
     * Does nothing 
     * 
     * @return always true
     */
    bool TurnOnInterrupts() { return true; }
    /** 
     * Does nothing 
     * 
     * @return always true
     */
    bool TurnOnTriggers()   { return true; }
    /** 
     * Does nothing 
     * 
     * @return always true
     */
    bool SendConfigured()   { return true; }

    void PrintSettings();
  };

}
#endif
//
// EOF
//


