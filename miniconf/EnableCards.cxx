#include "EnableCards.h"
#include <rcuxx/Rcu.h>
#include <rcuxx/rcu/RcuACTFEC.h>
#include <rcuxx/rcu/RcuCommand.h>
#include <unistd.h>


#define PP(T,X) std::setw(50) << T << ": " << X << '\n' 
#define PU(T,X,U) std::setw(50) << T << ": " << X << U << '\n'

//____________________________________________________________________
void
MiniConf::EnableCards::PrintSettings()
{
  std::ostream& o = std::cout;
  o << "FMD" << DetectorNumber() 
    << " [" << Timestamp() << "]: "
    << " Settings for this configuration:\n" 
    << std::boolalpha 
    << std::left
    << PP("Number of retries",                           fNRetry)
    << PU("Wait delay",                                  fWaitDelay, "ms")
    << PP("Detector number",                             fDetectorNumber)
    << std::noboolalpha 
    << std::right
    << std::endl;
}

//____________________________________________________________________
bool 
MiniConf::EnableCards::TurnOnCards(unsigned int cards)
{
  Rcuxx::RcuACTFEC*   actfec  = fRcu.ACTFEC();
  // Rcuxx::RcuCommand*  rstfec  = fRcu.FECRST();
  // Rcuxx::RcuCommand*  conffec = fRcu.CONFFEC();
  actfec->SetValue(cards);
  if (!CommitRegister(actfec, true)) return false;

  Wait();
  // Read back ACT FEC register 
  if (!DoVerifyRegister(actfec)) return false;

  return true;
}

//____________________________________________________________________
//
// EOF
//
