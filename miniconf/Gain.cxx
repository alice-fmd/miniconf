#include "Gain.h"
#include "config.h"
#include <rcuxx/Fmd.h>
#include <rcuxx/fmd/FmdPulser.h>
#include <rcuxx/fmd/FmdCalIter.h>
#include <rcuxx/fmd/FmdRange.h>
#include <rcuxx/bc/BcCommand.h>
#include <rcuxx/Altro.h>
#include <rcuxx/altro/AltroTRCFG.h>

//____________________________________________________________________
unsigned int 
MiniConf::Gain::CalcSamples() const
{
  unsigned int n = 4;
  switch (fRatio) { 
  case 1: 
  case 2: 
  case 4:
    return n * fRatio;
  }
  return 4 * n;
}

//____________________________________________________________________
bool
MiniConf::Gain::ConfigureALTROs()
{
  if (!Configuration::ConfigureALTROs()) return false;

  Rcuxx::AltroTRCFG* trcfg = fAltro.TRCFG();
  trcfg->SetACQ_START(0);
  trcfg->SetACQ_END(CalcSamples());
  return CommitRegister(trcfg);
}


//____________________________________________________________________
bool
MiniConf::Gain::ConfigureBCs()
{
  if (!Configuration::ConfigureBCs()) return false;
  if (!dynamic_cast<Rcuxx::Fmd*>(&fBc)) { 
    Error("BC is not an FMD BC!\n");
    return false;
  }
  Rcuxx::Fmd& fmd = dynamic_cast<Rcuxx::Fmd&>(fBc);
  
  Rcuxx::FmdPulser*   pulser  = fmd.Pulser();
  Rcuxx::FmdCalIter*  caliter = fmd.CalIter();
  Rcuxx::BcCommand*   calrun  = fmd.CalibrationRun();
  Rcuxx::FmdRange*    range   = fmd.Range();

  
  pulser->SetValue(0);
  pulser->SetTest(fGainStep);
  caliter->SetValue(fNGainSamples);
  range->SetMin(fMinStrip);
  range->SetMax(fMaxStrip);
  
  if (!CommitRegister(pulser))  return false;
  if (!CommitRegister(caliter)) return false;
  if (!CommitRegister(range))   return false;
  if (!CommitCommand(calrun))   return false;
  return true;
}

//____________________________________________________________________
bool 
MiniConf::Gain::ReadConfigurationBC(unsigned int id)
{
  if (!MiniConf::Configuration::ReadConfigurationBC(id)) return false;

  if (!dynamic_cast<Rcuxx::Fmd*>(&fBc)) { 
    Error("BC is not an FMD BC!\n");
    return false;
  }
  Rcuxx::Fmd&        fmd = dynamic_cast<Rcuxx::Fmd&>(fBc);
  Rcuxx::BcCommand*  calrun  = fmd.CalibrationRun();
  if (!ReadCommand(calrun)) return false;

  return true;
}

//____________________________________________________________________
//
// EOF
//
