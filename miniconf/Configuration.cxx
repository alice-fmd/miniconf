#include "config.h"
#include "Configuration.h"
#include <rcuxx/compat.h>
#include <rcuxx/Rcu.h>
#include <rcuxx/RegisterGuard.h>
#include <rcuxx/rcu/RcuACTFEC.h>
#include <rcuxx/rcu/RcuALTROIF.h>
#include <rcuxx/rcu/RcuACL.h>
#include <rcuxx/rcu/RcuCommand.h>
#include <rcuxx/rcu/RcuTRCFG1.h>
#include <rcuxx/rcu/RcuTRGCONF.h>
#include <rcuxx/rcu/RcuRDOMOD.h>
#include <rcuxx/rcu/RcuINTMOD.h>
#include <rcuxx/rcu/RcuL1Timeout.h>
#include <rcuxx/rcu/RcuL2Timeout.h>
#include <rcuxx/rcu/RcuRoiTimeout.h>
#include <rcuxx/rcu/RcuL1MsgTimeout.h>
#include <rcuxx/rcu/RcuTTCControl.h> 
#include <rcuxx/rcu/RcuROIConfig1.h>
#include <rcuxx/rcu/RcuROIConfig2.h>
#include <rcuxx/rcu/RcuALTROCFG1.h> 
#include <rcuxx/rcu/RcuALTROCFG2.h> 
#include <rcuxx/rcu/RcuRCUID.h> 
#include <rcuxx/rcu/RcuSCCLK.h>
#include <rcuxx/Bc.h>
#include <rcuxx/bc/BcCSR0.h>
#include <rcuxx/bc/BcCommand.h>
#include <rcuxx/Fmd.h>
#include <rcuxx/fmd/FmdHoldWait.h>
#include <rcuxx/fmd/FmdL0Timeout.h>
#include <rcuxx/fmd/FmdL1Timeout.h>
#include <rcuxx/fmd/FmdRange.h>
#include <rcuxx/fmd/FmdShiftClock.h>
#include <rcuxx/fmd/FmdSampleClock.h>
#include <rcuxx/fmd/FmdPulser.h>
#include <rcuxx/fmd/FmdCalIter.h>
#include <rcuxx/fmd/FmdMeb.h>
#include <rcuxx/fmd/FmdT1.h>
#include <rcuxx/fmd/FmdT2.h>
#include <rcuxx/fmd/FmdT3.h>
#include <rcuxx/fmd/FmdT4.h>
#include <rcuxx/fmd/FmdT1SENS.h>
#include <rcuxx/fmd/FmdT2SENS.h>
#include <rcuxx/fmd/FmdAL_DIG_I.h>
#include <rcuxx/fmd/FmdVA_REC_IP.h>
#include <rcuxx/fmd/FmdVA_SUP_IP.h>
#include <rcuxx/fmd/FmdVA_REC_IM.h>
#include <rcuxx/fmd/FmdFLASH_I.h>
#include <rcuxx/Altro.h>
#include <rcuxx/altro/AltroDPCFG.h>
#include <rcuxx/altro/AltroTRCFG.h>
#include <rcuxx/altro/AltroZSTHR.h>
#include <rcuxx/altro/AltroVFPED.h>
#include <rcuxx/altro/AltroRegister.h>
#include <rcuxx/altro/AltroCommand.h>
#include <cstdarg>
#include <fstream>
#define DO_VARIADIC(BUF,N,MSG) \
 va_list ap; va_start(ap,MSG); vsnprintf(BUF,N,MSG,ap); va_end(ap)

//____________________________________________________________________
std::string
MiniConf::Configuration::Timestamp() const
{
  time_t     now     = time(NULL);
  struct tm* loctime = localtime(&now);
  char tbuf[256];
  strftime(tbuf, 256, "%d %b %Y %T", loctime);
  return std::string(tbuf);
}



//____________________________________________________________________
void
MiniConf::Configuration::Error(const char* msg, ...)
{
  static char buf[1024];
  DO_VARIADIC(buf, 1024, msg);
  if (fZenityOutput) std::cerr << "# ";
  else               std::cerr << "FMD" << DetectorNumber() << " [" 
			       << Timestamp() << "]: ";
  std::cerr << buf << std::flush;
  if (fLog) *fLog << "FMD" << DetectorNumber() << " [" << Timestamp() << "]: " 
		  << buf << std::flush;
}
//____________________________________________________________________
void
MiniConf::Configuration::Warn(const char* msg, ...)
{
  static char buf[1024];
  DO_VARIADIC(buf, 1024, msg);
  if (fZenityOutput) std::cerr << "# ";
  else               std::cerr << "FMD" << DetectorNumber() << " [" 
			       << Timestamp() << "]: ";
  std::cerr << buf << std::flush;
  if (fLog) *fLog << "FMD" << DetectorNumber() << " [" << Timestamp() << "]: " 
		  << buf << std::flush;
}
//____________________________________________________________________
void
MiniConf::Configuration::Info(const char* msg, ...)
{
  static char buf[1024];
  DO_VARIADIC(buf, 1024, msg);
  if (fZenityOutput) std::cout << "# ";
  else               std::cout << "FMD" << DetectorNumber() 
			       << " [" << Timestamp() << "]: ";
  std::cout << buf << std::flush;
  if (fLog) *fLog << "FMD" << DetectorNumber() << " [" << Timestamp() << "]: " 
		  << buf << std::flush;
}

//____________________________________________________________________
bool 
MiniConf::Configuration::IsBCOn(unsigned int address)
{
  Rcuxx::RcuACTFEC* actfec = fRcu.ACTFEC();
  return actfec->IsOn(address);
}
//____________________________________________________________________
bool 
MiniConf::Configuration::IsALTROOn(unsigned int board, unsigned int chip)
{
  Rcuxx::RcuACL* acl = fRcu.ACL();
  return acl->CheckChip(board, chip) != 0x0;
}

//____________________________________________________________________
bool 
MiniConf::Configuration::DoCommitRegister(Rcuxx::Register* reg, bool verify)
{
  if (!reg) return true;
  Info("=== Writing register %s\n", reg->Name().c_str());
  unsigned int i = 0;
  unsigned int ret = 0;
  for (i = 0; i < fNRetry; i++) {
    Wait(10);
    ret = reg->Commit();
    if (ret) continue;
    Wait(10);
    if (verify && !DoVerifyRegister(reg)) continue;
    i++;
    break;
  }
  Info(">>> Writing %s %s, %d tries\n", reg->Name().c_str(), 
       (ret ? " failed": "ok"), i);
  if (ret) Error("Failed to write %s: %s\n", 
		 reg->Name().c_str(), fRcu.ErrorString(ret).c_str());
  return ret == 0;
}
//____________________________________________________________________
bool 
MiniConf::Configuration::CommitRegister(Rcuxx::AltroRegister* reg, bool verify)
{
  if (!reg) return false;

  bool ret = DoCommitRegister(reg, verify); 
  if (!ret || !fEnableRewrite) return ret; 

  Info("=== Storing write instructions for %s\n", reg->Name().c_str());
  int errval = reg->WriteInstructions(fImemCache, fImemMarker);
  if (errval != 0) {
    Error(">>> Failed to store write instructions for %s: %d - %s\n", 
	  reg->Name().c_str(), errval, fRcu.ErrorString(errval).c_str());
    ret = false;
  }

  return ret;
}
//____________________________________________________________________
bool 
MiniConf::Configuration::DoVerifyRegister(Rcuxx::Register* reg)
{
  if (!reg) return true;
  Info("??? Verifying register %s\n", reg->Name().c_str());
  unsigned int old = reg->Encode();
  bool         ret = reg->Verify();
  Info(">>> Verifying %s %s\n", reg->Name().c_str(), (!ret ? " failed": "ok"));
  if (!ret) { 
    int          ret2 = reg->Update();
    unsigned int cur  = reg->Encode();
    Error("Failed to verify %s - expected 0x%08x, got 0x%08x: %s\n", 
	  reg->Name().c_str(), old, cur, fRcu.ErrorString(ret2).c_str());
  }
  return ret;
}

//____________________________________________________________________
bool 
MiniConf::Configuration::DoCommitCommand(Rcuxx::Command* reg)
{
  if (!reg) return true;
  Info("=== Executing command %s\n", reg->Name().c_str());
  unsigned int i = 0;
  unsigned int ret = 0;
  for (i = 0; i < fNRetry; i++) {
    Wait(10);
    ret = reg->Commit();
    if (ret) continue;
    i++;
    break;
  }
  //  Info("Command %s %s, %d tries\n", reg->Name().c_str(), 
  //       (ret ? " failed": "ok"), i);

  if (ret) Error("Failed to execute %s: %s\n", 
		 reg->Name().c_str(), fRcu.ErrorString(ret).c_str());
  return ret == 0;
}
//____________________________________________________________________
bool 
MiniConf::Configuration::CommitCommand(Rcuxx::AltroCommand* reg)
{
  if (!reg) return false;

  bool ret = DoCommitCommand(reg);
  if (!ret || !fEnableRewrite) return ret; 
  
  Info("=== Storing command instructions for %s\n", reg->Name().c_str());
  int errval = reg->WriteInstructions(fImemCache, fImemMarker);
  if (errval != 0) {
    Error(">>> Failed to store command instructions for %s: %d - %s\n", 
	  reg->Name().c_str(), errval, fRcu.ErrorString(errval).c_str());
    ret = false;
  }

  return ret;
}
//____________________________________________________________________
bool 
MiniConf::Configuration::ReadRegister(Rcuxx::AltroRegister* reg)
{
  Info("=== Reading register %s on SOD\n", reg->Name().c_str());
  // usleep(10);
  unsigned int i = 0;
  unsigned int ret = 0;
  for (i = 0; i < fNRetry; i++) {
    // Info(".");
    Wait(10);
    ret = reg->ReadInstructions();
    if (ret) continue;
    i++;
    break;
  }
  // Info("%s, %d tries\n", (ret ? " failed": "ok"), i);
  if (ret) {
    Error("Failed to write read instruction of %s: %s\n",
	  reg->Name().c_str(), fRcu.ErrorString(ret).c_str());
    return false;
  }
  // Wait 4 clock cycles.
  unsigned int wait[] = { 0x320004 };
  fRcu.IMEM()->Append(wait, 1);
  return true;
}
//____________________________________________________________________
bool 
MiniConf::Configuration::ReadCommand(Rcuxx::AltroCommand* reg)
{
  Info("=== Reading command %s result on SOD\n", reg->Name().c_str());
  // usleep(10);
  unsigned int ret = 0;
  unsigned int i = 0;
  for (i = 0; i < fNRetry; i++) {
    // Info(".");
    Wait(10);
    ret = reg->WriteInstructions();
    if (ret) continue;
    i++;
    break;
  }
  // Info("%s, %d tries\n", (ret ? " failed": "ok"), i);
  if (ret) { 
    Error("Failed to write execution instruction of %s: %s\n",
	  reg->Name().c_str(), fRcu.ErrorString(ret).c_str());
    return false;
  }
  return true;
}

//____________________________________________________________________
MiniConf::Configuration::~Configuration()
{
  if (fLog) delete fLog;
}

//____________________________________________________________________
void 
MiniConf::Configuration::EnableLogfile(const std::string& logfile)
{
  if (logfile.empty()) return;
  fLog = new std::ofstream(logfile.data());
}

//____________________________________________________________________
void 
MiniConf::Configuration::SetOversampling(unsigned int r)
{
  switch (r) { 
  case 1: 
  case 2: 
  case 4: fRatio = r; break;
  default: 
    Error("Invalid oversampling rate: %d, must be one of 1, 2, or 4", r);
    fRatio = 4;
  }
}

#define PP(T,X) std::setw(50) << T << ": " << X << '\n' 
#define PU(T,X,U) std::setw(50) << T << ": " << X << U << '\n'

//____________________________________________________________________
void
MiniConf::Configuration::PrintSettings()
{
  std::ostream& o = std::cout;
  o << "FMD" << DetectorNumber() 
    << " [" << Timestamp() << "]: "
    << " Settings for this configuration:\n" 
    << std::boolalpha 
    << std::left
    << PP("Enable SOD event",                            fEnableSOD)
    << PP("Re-write registers on SOD",                   fEnableRewrite)
    << PP("Enable triggers",                             fEnableTriggers)
    << PP("Enable monitoring",                           fEnableMonitoring)
    << PP("Enable interrupts",                           fEnableInterrupts)
    << PP("Enable interleaved branch read-out",          fInterleaved)
    << PP("Enable zero suppression",                     fZeroSuppress)
    << PP("Zero suppression threshold in ADC counts",    fZSThreshold)
    << PP("Number of retries",                           fNRetry)
    << PP("Work around FMD3 bug",                        fFmd3Workaround)
    << PP("Output suitable for Zenity",                  fZenityOutput)
    << PP("Oversampling rate",                           fRatio)
    << PP("Number of buffers",                           fNMeb)
    << PP("Interrupt groups",                            fIntGroups)
    << PU("The delay between ACTFEC and CONFFEC",        fConfDelay, "s")
    << PU("Wait delay",                                  fWaitDelay, "ms")
    << PP("number of samples per pulser value per strip",fNGainSamples)
    << PP("Step size in gain pulser",                    fGainStep)
    << PP("First strip to read out",                     fMinStrip)
    << PP("Last strip to read out",                      fMaxStrip)
    << PP("Detector number",                             fDetectorNumber)
    << PP("L1 window center - RCU setting",              fL1Window)
    << PP("L0 timeout - BC setting",                     fL0Timeout)
    << PP("Instruction memory cache size",               fImemCache.size())
    << PP("Current pointer into IMEM cache",             fImemMarker)
    << std::noboolalpha 
    << std::right
    << std::endl;
    
    

}

//____________________________________________________________________
bool 
MiniConf::Configuration::Execute(unsigned int cards)
{
  bool ret = false;
  PrintSettings();
  try {
    // Some sanity checks 
    if (fZeroSuppress) 
      // Ensure proper settings for zero-suppression.
      fZSThreshold = std::max(2, int(fZSThreshold));
    else 
      fZSThreshold = 0;
    // Resize our IMEM cache if so enabled 
    if (fEnableRewrite) { 
      fImemCache.resize(fRcu.IMEM()->Size(),0);
    }

    // Then, call the concrete implementation for the BCs
    if (fZenityOutput) std::cout << "0" << std::endl;
    fRcu.ExecCommand(0xF002); // Turn off monitoring 
    if (!ConfigureRCU()) { 
      Error("Failed to configure RCU");
      throw false;
    }
  
    // First, turn on the front-end cards. 
    if (fZenityOutput) std::cout << "10" << std::endl;
    if (!TurnOnCards(cards)) { 
      Error("Failed to turn on cards 0x%08x\n", cards);
      throw false;
    }
  
    // Then, call the concrete implementation for the BCs
    if (fZenityOutput) std::cout << "40" << std::endl;
    fBc.SetBroadcast();
    if (!ConfigureBCs()) { 
      Error("Failed to configure BCs (common)\n");
      throw false;
    }
  
    // Then, call the concrete implementation for the BCs
    if (fZenityOutput) std::cout << "50" << std::endl;
    fAltro.SetBroadcast();
    if (!ConfigureALTROs()) { 
      Error("Failed to configure ALTROs (common)\n");
      throw false;
    }
    
    // Configure individual FECs 
    if (fZenityOutput) std::cout << "60" << std::endl;
    Info("*** Configuring FECs: \n");
    for (size_t i = 0; i < 32; i++) { 
      if (!IsBCOn(i)) continue;
      Info("  BC 0x%02x:\n", i);
      fBc.SetAddress(i);
      if (!ConfigureBC(i)) { 
	Error("Failed to configure BC 0x%02x\n", i);
	throw false;
      }
      for (size_t j = 0; j < 8; j++) { 
	if (!IsALTROOn(i, j)) continue;
	Info("    ALTRO 0x%02x/0x%1x\n", i, j);
	fAltro.SetAddress(i, j, 0);
	if (!ConfigureALTRO(i, j)) { 
	  Error("Failed to configure ALTRO 0x%02x/0x%1x\n", i, j);
	  throw false;
	}
      }
      Info("done\n");
    }

    // Enable interrupts
    if (fZenityOutput) std::cout << "70" << std::endl;
    if (!TurnOnInterrupts()) { 
      Error("Failed to enable interrupts\n");
      throw false;
    }

    // Read configuration on SOD
    if (fZenityOutput) std::cout << "80" << std::endl;
    if (!ReadConfiguration()) {
      Error("Failed to set-up for read of configuration on SOD event");
      throw false;
    }
  
    // Enable triggers from TTC 
    if (fZenityOutput) std::cout << "90" << std::endl;
    if (!TurnOnTriggers()) { 
      Error("Failed to enable TTC triggers\n");
      throw false;
    }
  
    // Now, send configure command (even though it's empty)
    if (fZenityOutput) std::cout << "95" << std::endl;
    if (!SendConfigured()) { 
      Error("Failed to send configure command\n");
      throw false;
    }

    Info("Configuration done\n");
    if (fZenityOutput) std::cout << "100" << std::endl;
    ret = true;
  }
  catch (bool e) {
    ret = e;
  }
  fRcu.ExecCommand(0xF001); // Turn on monitoring 
  return ret;
}

//____________________________________________________________________
bool 
MiniConf::Configuration::SendConfigured()
{
  // Do not do anything here - we do not need it, since the state is
  // derived from the hardware
  return true;

  // Old code
  Info("*** Sending configured to RCU\n");
  unsigned int ret = 0;
  fRcu.StartBlock();
  if ((ret = fRcu.EndBlock()) != 0) { 
    Error("Failed to send configured command: %s\n", 
	  fRcu.ErrorString(ret).c_str());
    return false;
  }
  return true;
}

//____________________________________________________________________
unsigned int 
MiniConf::Configuration::CalcSamples() const
{
  unsigned int n = 128 + 8;
  switch (fRatio) {
  case 1:
  case 2: 
  case 4: n *= fRatio;
    break;
  default: 
    n *= 4;
  }
  return n;
}

//____________________________________________________________________
bool 
MiniConf::Configuration::ResetRCU()
{
  // Reset FSMs in RCU 
  try { 
    // Sicne RCU_RESET also resets the TTC register, we 
    // need to cache these here.  We do that using a 
    // guard pattern.  The guards throw execptions on 
    // error, so we need to do this in a try- catch block 
    Rcuxx::RegisterGuard g1(fRcu.TTCControl());
    Rcuxx::RegisterGuard g2(fRcu.ROIConfig1());
    Rcuxx::RegisterGuard g3(fRcu.ROIConfig2());
    Rcuxx::RegisterGuard g4(fRcu.L1Timeout());
    Rcuxx::RegisterGuard g5(fRcu.L2Timeout());
    Rcuxx::RegisterGuard g6(fRcu.RoiTimeout());
    Rcuxx::RegisterGuard g7(fRcu.L1MsgTimeout());

    bool resetTTC   = fRcuResetFlags & 0x4;
    bool resetRCU   = fRcuResetFlags & 0x2;
    bool syncClocks = fRcuResetFlags & 0x1;

    if (resetRCU   && !CommitCommand(fRcu.RCU_RESET()))  return false;
    if (syncClocks && !CommitCommand(fRcu.ARM_ASYNCH())) return false;
    if (resetTTC   && !CommitCommand(fRcu.TTCReset()))   return false;
    
  }
  catch (const std::exception& e)  {
    Error("Got an exception: %s", e.what());
    return false;
  }
  return true;
}
//____________________________________________________________________
bool 
MiniConf::Configuration::ConfigureRCU()
{
  Info("*** Configuring RCU\n");
  if (fRcu.TRCFG1()) { 
    Rcuxx::RcuTRCFG1* trcfg1 = fRcu.TRCFG1();
    trcfg1->SetMode(Rcuxx::RcuTRCFG1::kDerivedL2); // kExternal
    trcfg1->SetTwv(4096);
    trcfg1->SetBMD(Rcuxx::RcuTRCFG1::k4Buffers);
    if (!CommitRegister(trcfg1)) return false;
  }
  if (fRcu.TRGCONF()) { 
    Rcuxx::RcuTRGCONF* trgconf = fRcu.TRGCONF();
    trgconf->SetMapping(Rcuxx::RcuTRGCONF::kL1ToL1);
    trgconf->SetL2Latency(4096);
    // Disable triggers here - force us to go to standby. 
    trgconf->EnableTTCTriggers(false);
    if (!CommitRegister(trgconf, true)) return false;
  }
  if (fRcu.RDOMOD()) {
    Rcuxx::RcuRDOMOD* rdomod = fRcu.RDOMOD();
    rdomod->SetBuffers(Rcuxx::RcuRDOMOD::k4Buffers);
    rdomod->SetSparseReadout(false);
    rdomod->SetCheckRDYRX(false);
    rdomod->SetExecOnSOD(fEnableSOD);
    if (!CommitRegister(rdomod,true)) return false;
  }
  if (fRcu.ALTROIF()) {
    Rcuxx::RcuALTROIF* altroif = fRcu.ALTROIF();
    altroif->Update();
    altroif->SetBcCheck(Rcuxx::RcuALTROIF::kFMDBC);
    altroif->SetBcCheck(Rcuxx::RcuALTROIF::kNoCheck);
    altroif->SetCSTBDelay(3);
    altroif->SetSamplesPerChannel(CalcSamples());
    switch (fRatio) { 
    case 1:  
      altroif->SetSamplingRatio(Rcuxx::RcuALTROIF::k2500kHz); break;
    case 2:  
      altroif->SetSamplingRatio(Rcuxx::RcuALTROIF::k5MHz); break;
    default: 
      altroif->SetSamplingRatio(Rcuxx::RcuALTROIF::k10MHz); break;
    }
    if (!CommitRegister(altroif,true)) return false;
  }
  if (fRcu.ALTROCFG1()) {
    /* Note, previously, the ALTROCFG1 register was interpreted as 
     * 
     * Bits    Value    Description
     *   0- 3      0/1   1st Baseline filter, mode 
     *   4- 5   Over-1   2nd baseline filter, # of pre-samples
     *   6- 9   factor   2nd baseline filter, # of post-samples 
     *  10-          0   2nd baseline filter, enable
     *  11-12       00   Zero suppression, glitch filter mode
     *  13-15      001   Zero suppression, # of post samples
     *  16-17       01   Zero suppression, # of pre  samples
     *  18         0/1   Zero suppression, enable
     *
     * The interpretation used in AliAltroRawStreamerV3 - which
     * corresponds directly to ALTRO DPCFG register - is 
     *
     * Bits    Value  Description
     *   0- 3    0/1   1st Baseline filter, mode 
     *   4         0   Polarity (if '1', then "1's inverse")
     *   5- 6     01   Zero suppression, # of pre samples
     *   7-10   0001   Zero suppression, # of post samples
     *  11         0   2nd baseline filter, enable
     *  12-13     00   Zero suppression, glitch filter mode
     *  14-16 factor   2nd baseline filter, # of post-samples
     *  17-18     01   2nd baseline filter, # of pre-samples 
     *  19       0/1   Zero suppression, enable
     *
     *  Writing 'x' for variable values, that means we have the
     *  following patterns for the 2 cases 
     *
     *    bit #  20   16   12    8    4    0
     *     old    |0x01|0010|00xx|xxxx|xxxx|
     *     new    |x01x|xx00|0000|1010|xxxx|
     *
     *  That means that we can check if bits 10-13 are '1000' or
     *  '0000', which will tell us if the value was written with the
     *  new or the old interpretation.    That is, we can check that 
     *
     *    if (((altrocfg1 > 10) & 0x8) == 0x8) { 
     *      // old interpretation 
     *    }
     *    else { 
     *      // New interpretation 
     *    }
     *
     * That means, that we should never 
     *
     *  - change the # of zero suppression post samples 
     *  - Turn on 2nd baseline correction 
     *  - Change the zero-suppression glitch filter mode
     *
     * This change as introduced in version 1.2 of Rcu++
     */
    Rcuxx::RcuALTROCFG1* altrocfg1 = fRcu.ALTROCFG1();
    altrocfg1->SetFirstBMode(fZeroSuppress ? 1 : 0);
    altrocfg1->SetSecondBEnable(false);
    altrocfg1->SetSecondBPre(int(fRatio-1) & 0x3);
    altrocfg1->SetZSEnable(fZeroSuppress);
    altrocfg1->SetZSPre(1);
    altrocfg1->SetZSPost(1);
    if (!CommitRegister(altrocfg1,true)) return false;
  }
  if (fRcu.ALTROCFG2()) {
    Rcuxx::RcuALTROCFG2* altrocfg2 = fRcu.ALTROCFG2();
    // WARNING:  Using # of presamples to encode the oversampling
    // ratio
    altrocfg2->SetPreSamples(fRatio);
    if (!CommitRegister(altrocfg2,true)) return false;
  }
  if (fRcu.L1Timeout()) {
    int d  = DetectorNumber();
    int to = fL1Window; // (d == 0 ? 224 : 260); // Special for lab-setup
    Rcuxx::RcuL1Timeout* l1timeout = fRcu.L1Timeout();
    l1timeout->SetTimeout(to); // 0xe0);
    l1timeout->SetWindow(2);
    std::cout << "FMD" << d << " L1 timeout set to " 
	<< to << " (" << fL1Window << ")" << std::endl;
    if (!CommitRegister(l1timeout,true)) return false;
    l1timeout->Print();
  }
  if (fRcu.TTCControl()) {  
    Rcuxx::RcuTTCControl* ttccontrol = fRcu.TTCControl();
    ttccontrol->Update();
    ttccontrol->SetL0Support(1);
    if (!CommitRegister(ttccontrol,true)) return false;
  }
  if (fRcu.RCUID()) {  
    Rcuxx::RcuRCUID* rcuId = fRcu.RCUID();
    rcuId->SetId(DetectorNumber());
    if (!CommitRegister(rcuId,false)) return false;
  }
  if (fRcu.SCCLK()) {
    Rcuxx::RcuSCCLK* scClk = fRcu.SCCLK();
    scClk->SetSpeed(Rcuxx::RcuSCCLK::k2500kHz);
    if (!CommitRegister(scClk, true)) return false;
  }
  else 
    Warn("=== No access to SCCLK - perhaps via DDL, check sc clock speed\n");
  return true;
}

//____________________________________________________________________
bool
MiniConf::Configuration::ConfigureALTROs()
{
  Info("*** Configuring ALTROs\n");
  Rcuxx::AltroDPCFG* dpcfg = fAltro.DPCFG();
  dpcfg->SetFirstBMode((fZeroSuppress ? 1 : 0)); 
  dpcfg->SetSecondBEnable(false);
  dpcfg->SetZSEnable(fZeroSuppress);
  dpcfg->SetZSPre(1);
  dpcfg->SetZSPost(1);
  if (!CommitRegister(dpcfg)) return false;

  // set the fixed pedestal - normally 0, but Kris may want to change it.
  Rcuxx::AltroVFPED* vfped = fAltro.VFPED();
  vfped->SetFP(0);
  if (!CommitRegister(vfped)) return false;

  Rcuxx::AltroZSTHR* zsthr = fAltro.ZSTHR();
  zsthr->SetZS_THR(fZSThreshold);
  zsthr->SetOffset(0);
  if (!CommitRegister(zsthr)) return false;
  
  // Maybe set this depending on clocks 
  Rcuxx::AltroTRCFG* trcfg = fAltro.TRCFG();
  trcfg->SetACQ_START(0);
  trcfg->SetACQ_END(CalcSamples());
  if (!CommitRegister(trcfg)) return false;

  Info("*** Done configuring ALTROs\n");
  return true;
}

//____________________________________________________________________
bool
MiniConf::Configuration::ConfigureBCs()
{
  Info("*** Configuring BCs\n");
  if (!dynamic_cast<Rcuxx::Fmd*>(&fBc)) { 
    Error("BC is not an FMD BC!\n");
    return false;
  }
  Rcuxx::Fmd& fmd = dynamic_cast<Rcuxx::Fmd&>(fBc);
  

  Rcuxx::FmdHoldWait*     holdwait    = fmd.HoldWait();
  if (holdwait) { 
    holdwait->SetClocks(32);
    if (!CommitRegister(holdwait))    return false;
  }

  Rcuxx::FmdL0Timeout*    l0timeout   = fmd.L0Timeout();
  if (l0timeout) { 
    l0timeout->SetClocks(fL0Timeout); // 290);   // Longer than default
    if (!CommitRegister(l0timeout))   return false;
  }

  Rcuxx::FmdL1Timeout*    l1timeout   = fmd.L1Timeout();
  if (l1timeout) { 
    l1timeout->SetClocks(5000); // Longer than default  
    if (!CommitRegister(l1timeout))   return false;
  }

  Rcuxx::FmdRange*        range       = fmd.Range();
  if (range) { 
    range->SetMin(0);
    range->SetMax(127);
    if (!CommitRegister(range))       return false;
  }

  Rcuxx::FmdShiftClock*   shiftclock  = fmd.ShiftClock();
  if (shiftclock) { 
    shiftclock->SetDivision(16);
    shiftclock->SetPhase(fRatio == 1 ? 14 : 2);
    if (!CommitRegister(shiftclock))  return false;
  }

  Rcuxx::FmdSampleClock*  sampleclock = fmd.SampleClock();
  if (sampleclock) { 
    sampleclock->SetDivision(fRatio == 4 ? 4 : 
			     fRatio == 2 ? 8 :
			     fRatio == 1 ? 16 : 4);
    sampleclock->SetPhase(2);
    if (!CommitRegister(sampleclock)) return false;
  }

  Rcuxx::FmdMeb* meb = fmd.Meb();
  if (meb) { 
    meb->SetEnabled(fNMeb > 0);
    meb->SetMaximum(fNMeb);
    if (!CommitRegister(meb)) return false;
  }


  Rcuxx::BcCSR0* csr0 = fmd.CSR0();
  if (csr0) { 
    csr0->SetParity(true);
    csr0->SetInstruction(true);
    csr0->SetMissedSclk(false);
    csr0->SetAlps(true);
    csr0->SetPaps(true);
    csr0->SetDcOverTh(false);
    csr0->SetAcOverTh(false);
    csr0->SetDcOverTh(false);
    csr0->SetAvOverTh(false);
    csr0->SetTempOverTh(true);
    
    if (!CommitRegister(csr0)) return false;
  }
  

  Info("*** Done configuring BCs\n");

  int d = DetectorNumber();
  if (d != 0) return true;
  
  Info("*** Increasing temperature limits in BCs\n");

  Rcuxx::FmdT1SENS_TH*    t1sens_th   = fmd.T1SENS_TH();
  Rcuxx::FmdT2SENS_TH*    t2sens_th   = fmd.T2SENS_TH();
  Rcuxx::FmdT1_TH*    	  t1_th       = fmd.T1_TH();
  Rcuxx::FmdT2_TH*        t2_th       = fmd.T2_TH();
  Rcuxx::FmdT3_TH*        t3_th       = fmd.T3_TH();
  Rcuxx::FmdT4_TH*        t4_th       = fmd.T4_TH();

  t1sens_th->SetThreshold(55.f); 
  t2sens_th->SetThreshold(55.f);
  t1_th->SetThreshold(50.f); 
  t2_th->SetThreshold(50.f); 
  t3_th->SetThreshold(50.f); 
  t4_th->SetThreshold(50.f); 
  if (!CommitRegister(t1sens_th))      return false;
  if (!CommitRegister(t2sens_th))      return false;
  if (!CommitRegister(t1_th))      return false;
  if (!CommitRegister(t2_th))      return false;
  if (!CommitRegister(t3_th))      return false;
  if (!CommitRegister(t4_th))      return false;


  return true;
}

//____________________________________________________________________
bool 
MiniConf::Configuration::ConfigureBC(unsigned int)
{
  // Enable monitoring
  if (!TurnOnMonitoring()) { 
    Error("Failed to turn on monitoring on BCs\n");
    return false;
  }
  return true;
}
//____________________________________________________________________
bool 
MiniConf::Configuration::ConfigureALTRO(unsigned int, unsigned int)
{
  return true;
}

//____________________________________________________________________
bool 
MiniConf::Configuration::ReadConfiguration()
{
  if (!fEnableSOD) return true;

  // Read from individual FECs 
  Info("*** Read from FECs: \n");
  fBc.SetBroadcast();
  ReadConfigurationBC();
  fAltro.SetBroadcast();
  ReadConfigurationALTRO();
  for (size_t i = 0; i < 32; i++) { 
    if (!IsBCOn(i)) continue;
    Info("--- Board 0x%02x:\n", i);
    fBc.SetAddress(i);
    if (!ReadConfigurationBC(i)) { 
      Error("Failed to read configure BC 0x%02x\n", i);
      return false;
    }
    
    for (size_t j = 0; j < 8; j++) { 
      if (!IsALTROOn(i, j)) continue;
      Info("___ Altro 0x%1x:\n", j);
      fAltro.SetAddress(i, j, 0);
      if (!ReadConfigurationALTRO(i, j)) { 
	Error("Failed to read configure ALTRO 0x%02x/0x%1x\n", i, j);
	return false;
      }
    }
    Info("*** Done reading configurations off BCs\n");
  }
  if (fEnableRewrite) { 
    Info("*** Appending re-write block of size %d to RCU\n", fImemMarker);
    fRcu.IMEM()->Append(&(fImemCache[0]), fImemMarker);
  }
  static unsigned int stop = fRcu.Layout().kIMEM_STOP;
  fRcu.IMEM()->Append(&stop, 1);
  fRcu.IMEM()->Print();
  unsigned int ret = 0;
  if ((ret = fRcu.IMEM()->Commit())) { 
    Error("Couldn't commit IMEM SOD block: %s\n", 
	  fRcu.ErrorString(ret).c_str());
    return false;
  }
  return true;
}

//____________________________________________________________________
bool 
MiniConf::Configuration::ReadConfigurationBC()
{
  return true;
}
//____________________________________________________________________
bool 
MiniConf::Configuration::ReadConfigurationALTRO()
{
  Rcuxx::AltroCommand* rpinc = fAltro.RPINC();
  if (!rpinc) return true;
  
  for (size_t i = 0; i < 8; i++) { 
    if (!ReadCommand(rpinc)) return false;
  }
  return true;
}

//____________________________________________________________________
bool 
MiniConf::Configuration::ReadConfigurationBC(unsigned int)
{
  if (!dynamic_cast<Rcuxx::Fmd*>(&fBc)) { 
    Error("BC is not an FMD BC!\n");
    return false;
  }
  Rcuxx::Fmd& fmd = dynamic_cast<Rcuxx::Fmd&>(fBc);
  Rcuxx::FmdRange*        range       = fmd.Range();
  Rcuxx::FmdShiftClock*   shiftclock  = fmd.ShiftClock();
  Rcuxx::FmdSampleClock*  sampleclock = fmd.SampleClock();
  Rcuxx::FmdPulser*       pulser      = fmd.Pulser();
  Rcuxx::FmdCalIter*      caliter     = fmd.CalIter();
  if (range       && !ReadRegister(range)) return false;
  if (sampleclock && !ReadRegister(sampleclock)) return false;
  if (shiftclock  && !ReadRegister(shiftclock)) return false;
  if (pulser      && !ReadRegister(pulser)) return false;
  if (caliter     && !ReadRegister(caliter)) return false;

  return true;
}
//____________________________________________________________________
bool 
MiniConf::Configuration::ReadConfigurationALTRO(unsigned int, unsigned int)
{
  // Test for Kris
  Rcuxx::AltroTRCFG* trcfg = fAltro.TRCFG();
  if (!ReadRegister(trcfg)) return false;
  Rcuxx::AltroZSTHR* zsthr = fAltro.ZSTHR();
  if (!ReadRegister(zsthr)) return false;


  return true;
}

//____________________________________________________________________
int
MiniConf::Configuration::DetectorNumber() const
{
  if (fDetectorNumber >= 0) return fDetectorNumber;
  const Rcuxx::Url& url    = fRcu.GetUrl();
  const std::string scheme = url.Scheme();
  const std::string p      = url.Path();
  if (scheme == "fee") { 
    char         l = p[p.size()-1];
    switch (l) { 
    case '1': return 1; break;
    case '2': return 2; break;
    case '3': return 3; break;
    }
  }
  if (scheme == "rorc") { 
    return 4; // Bogus number, which isn't zero
  }
  return 0;
}

//____________________________________________________________________
bool 
MiniConf::Configuration::TurnOnCards(unsigned int cards)
{
  Rcuxx::RcuACTFEC*   actfec  = fRcu.ACTFEC();
  Rcuxx::RcuCommand*  rstfec  = fRcu.FECRST();
  Rcuxx::RcuCommand*  conffec = fRcu.CONFFEC();
  actfec->SetValue(cards);
  if (!CommitRegister(actfec, true)) return false;

  if (fConfDelay > 0) { 
    Info("Sleeping for %d seconds before configuring BCs\n", fConfDelay);
    Wait(fConfDelay*1000);
  }
  if (fZenityOutput) std::cout << "20" << std::endl;
  if (!CommitCommand(conffec)) return false;
  // Now do N resets
  unsigned int nFECRST = 4;
  Info("Will do %d FECRST", nFECRST);
  for (unsigned int iFECRST = 0; iFECRST < nFECRST; iFECRST++) {
    Info("Reset FEC %d/%d", iFECRST+1, nFECRST);
    Wait();
    if (!CommitCommand(rstfec)) return false;
  }
  
  // Read back ACT FEC register 
  if (!DoVerifyRegister(actfec)) return false;
  
  if (fZenityOutput) std::cout << "30" << std::endl;
  int d = DetectorNumber();
  // std::cout << "Detector is " << d << std::endl;
  
  unsigned int mask[]  = { 0xFFFF, 0xFF, 0xFFFF, 0, 0, 0, 0, 0 };
  unsigned int mask3[] = { 0x0000, 0x00, 0x0000, 0, 0, 0, 0, 0 };
  unsigned int mask0[] = { 0xFFFF, 0xFF, 0x0000, 0, 0, 0, 0, 0 };
  Rcuxx::RcuACL*      acl    = fRcu.ACL();
  for (unsigned int b = 0; b < 32; b++) { 
    if (!IsBCOn(b)) continue;
    unsigned int* m = mask;
    if (fFmd3Workaround && d == 3 && b == 0) { 
      Warn("**********************************************************\n"
	   "* FMD3i bottom FMDD not read out due to RCU FW II bug(?) *\n"
	   "**********************************************************\n");
      m = mask3;
    }
    if (d == 0 && b == 0x10) 
      m = mask0;
    acl->EnableBoard(b, m, 3);
  }
  if (fInterleaved) acl->Optimize();
  Info("=== Writing %s (0x%0x)\n", acl->Name().c_str(), acl->Base());
  Wait(300000);
  unsigned int ret = acl->Commit();
  if (ret) {
    Error("Failed to write ACL: %s\n", fRcu.ErrorString(ret).c_str());
    return false;
  }
  Wait(300000);
  // acl->Update();
  // acl->Print();
  
  return true;
}

//____________________________________________________________________
bool 
MiniConf::Configuration::TurnOnMonitoring()
{
  Info("*** Turning %s monitoring for 0x%02x\n", 
       fEnableMonitoring ? "on" : "off",
       fBc.BoardAddress());
  static bool first;
  if (first) {
    if (fRcu.SCCLK()) {
      Info("!!! Re-apply low SC clock speed\n");
      Rcuxx::RcuSCCLK* scClk = fRcu.SCCLK();
      scClk->SetSpeed(Rcuxx::RcuSCCLK::k2500kHz);
      if (!CommitRegister(scClk, true)) return false;
    }
    first = false;
  }
  Rcuxx::BcCSR0* csr0 = fBc.CSR0();
  // Check if we're broadcasting 
  if (fBc.BoardAddress() >= 0) {
    unsigned int ret = csr0->Update();
    if (ret) {
      Error("Failed to update %s: %s\n", csr0->Name().c_str(), 
	    fRcu.ErrorString(ret).c_str());
      return false;
    }
    fRcu.RS_STATUS()->Commit();
  }
  csr0->SetTempOverTh(fIntGroups & kIntT);
  csr0->SetDcOverTh(fIntGroups & kIntDc);
  csr0->SetAcOverTh(fIntGroups & kIntAc);
  csr0->SetDvOverTh(fIntGroups & kIntDv);
  csr0->SetAvOverTh(fIntGroups & kIntAv);
  csr0->SetCnv(0);
  if (fEnableMonitoring) { 
    if (!CommitRegister(csr0)) 
      Warn("Failed to commit CSR0 register to BC 0x%2x\n", 
	   fBc.BoardAddress());
    Rcuxx::BcCommand* stcnv = fBc.STCNV();
    if (!CommitCommand(stcnv)) 
      Warn("Failed to do a single conversion\n");
    Info("*** Sleeping %d second\n", fWaitDelay);
    Wait();
    // Now commit with monitoring on 
    csr0->SetCnv(fEnableMonitoring);
  }

  return CommitRegister(csr0);
}

//____________________________________________________________________
bool 
MiniConf::Configuration::TurnOnInterrupts()
{
  Info("*** Turning %s interrupts\n", fEnableInterrupts ? "on" : "off");
  Rcuxx::RcuINTMOD* intmod = fRcu.INTMOD();
  intmod->SetBranchA(fEnableInterrupts);
  intmod->SetBranchB(fEnableInterrupts);
  return CommitRegister(intmod);
}

//____________________________________________________________________
bool 
MiniConf::Configuration::TurnOnTriggers()
{
  Info("*** Turning %s triggers\n", fEnableTriggers ? "on" : "off");
  Rcuxx::RcuCommand* l1ttc = fRcu.L1_TTC();
  Rcuxx::RcuCommand* l1cmd = fRcu.L1_CMD();
  if (l1ttc && fEnableTriggers)  return CommitCommand(l1ttc);
  if (l1cmd && !fEnableTriggers) return CommitCommand(l1cmd);

  Rcuxx::RcuTRGCONF* trgconf = fRcu.TRGCONF();
  if (!trgconf) { 
    Error("Couldn't find either L1_TTC nor TRGCONF\n");
    return false;
  }
  trgconf->Update();
  trgconf->EnableTTCTriggers(fEnableTriggers);
  
  return CommitRegister(trgconf, true);
}
//____________________________________________________________________
void 
MiniConf::Configuration::Wait(int ms) 
{
  if (ms < 0) ms = fWaitDelay;
  USLEEP(ms);
}

//____________________________________________________________________
//
// EOF
//
