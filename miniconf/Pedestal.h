// -*- mode: C++ -*-
#ifndef MINICONF_Pedestal
#define MINICONF_Pedestal
#include <miniconf/Configuration.h>


namespace MiniConf
{
  /** @brief Configure for a pedestal run.  
      @note Currently, this is the same as the basic set-up
      done by Configuration. Eventually, we'll probably do 
      something different here */
  class Pedestal : public Configuration
  {
  public:
    Pedestal(Rcuxx::Rcu& rcu, Rcuxx::Bc& bc, Rcuxx::Altro& altro)
      : Configuration(rcu, bc, altro)
    {
      fZeroSuppress = false;
    }
    void EnableZeroSuppression(bool) { }
  };
}
#endif
//
// EOF
//


