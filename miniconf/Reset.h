// -*- mode: C++ -*-
#ifndef MINICONF_Reset
#define MINICONF_Reset
#include <miniconf/Configuration.h>


namespace MiniConf
{
  /** @brief Configure for a reset run.  
      @note Currently, this is the same as the basic set-up
      done by Configuration. Eventually, we'll probably do 
      something different here */
  class Reset : public Configuration
  {
  public:
    Reset(Rcuxx::Rcu& rcu, Rcuxx::Bc& bc, Rcuxx::Altro& altro)
      : Configuration(rcu, bc, altro)
    {
    }
    bool TurnOnCards(unsigned int mask);
    bool ConfigureRCU();
    bool ConfigureALTROs();
    bool ConfigureBCs();
    bool ConfigureALTRO(unsigned int board, unsigned int chip);
    bool ConfigureBC(unsigned int board);
    bool ReadConfigurationALTRO(unsigned int board,unsigned int chip);
    bool ReadConfigurationBC(unsigned int board);
    bool TurnOnMonitoring() { return true; }
    bool TurnOnInterrupts() { return true; }
    bool TurnOnTriggers()   { return true; }
    bool SendConfigured()   { return true; }
  };
  inline bool Reset::ConfigureALTROs() { return true; }
  inline bool Reset::ConfigureBCs() { return true; }
  inline bool Reset::ConfigureALTRO(unsigned int, unsigned int) {return true;}
  inline bool Reset::ConfigureBC(unsigned) { return true; }
  inline bool Reset::ReadConfigurationBC(unsigned) { return true; }
  inline bool Reset::ReadConfigurationALTRO(unsigned int,unsigned int)
  {return true;}

}
#endif
//
// EOF
//


