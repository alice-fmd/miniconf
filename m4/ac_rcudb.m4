dnl -*- mode: Autoconf -*- 
dnl
dnl $Id: ac_rcudb.m4,v 1.1 2012-03-06 12:36:21 hehi Exp $ 
dnl  
dnl  ROOT generic rcudb framework 
dnl  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
dnl
dnl  This library is free software; you can redistribute it and/or 
dnl  modify it under the terms of the GNU Lesser General Public License 
dnl  as published by the Free Software Foundation; either version 2.1 
dnl  of the License, or (at your option) any later version. 
dnl
dnl  This library is distributed in the hope that it will be useful, 
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of 
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
dnl  Lesser General Public License for more details. 
dnl 
dnl  You should have received a copy of the GNU Lesser General Public 
dnl  License along with this library; if not, write to the Free 
dnl  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 
dnl  02111-1307 USA 
dnl
dnl AC_RCUDB([MINIMUM-VERSION 
dnl             [,ACTION-IF_FOUND 
dnl              [, ACTION-IF-NOT-FOUND]])
AC_DEFUN([AC_RCUDB],
[
    # AC_REQUIRE([AC_RCUXX])
    # Command line argument to specify prefix. 
    AC_ARG_WITH([rcudb-prefix],
        [AC_HELP_STRING([--with-rcudb-prefix],
		[Prefix where RcuDb is installed])],
        rcudb_prefix=$withval, rcudb_prefix="")

    # Command line argument to specify documentation URL. 
    AC_ARG_WITH([rcudb-url],
        [AC_HELP_STRING([--with-rcudb-url],
		[Base URL where the RcuDb dodumentation is installed])],
        rcudb_url=$withval, rcudb_url="")
    if test "x${RCUDB_CONFIG+set}" != xset ; then 
        if test "x$rcudb_prefix" != "x" ; then 
	    RCUDB_CONFIG=$rcudb_prefix/bin/rcudb-config
	fi
    fi   

    # Check for the configuration script. 
    AC_PATH_PROG(RCUDB_CONFIG, rcudb-config, no)
    rcudb_min_version=ifelse([$1], ,0.11,$1)
    
    # Message to user
    AC_MSG_CHECKING(for RcuDb version >= $rcudb_min_version)

    # Check if we got the script
    rcudb_found=no    
    if test "x$RCUDB_CONFIG" != "xno" ; then 
       # If we found the script, set some variables 
       RCUDB_CPPFLAGS=`$RCUDB_CONFIG --cppflags`
       RCUDB_INCLUDEDIR=`$RCUDB_CONFIG --includedir`
       RCUDB_LIBS=`$RCUDB_CONFIG --libs`
       RCUDB_LTLIBS=`$RCUDB_CONFIG --ltlibs`
       RCUDB_LIBDIR=`$RCUDB_CONFIG --libdir`
       RCUDB_LDFLAGS=`$RCUDB_CONFIG --ldflags`
       RCUDB_LTLDFLAGS=`$RCUDB_CONFIG --ltldflags`
       RCUDB_PREFIX=`$RCUDB_CONFIG --prefix`

       # Check the version number is OK.
       rcudb_version=`$RCUDB_CONFIG -V` 
       rcudb_vers=`echo $rcudb_version | \
         awk 'BEGIN { FS = "."; } \
	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
       rcudb_regu=`echo $rcudb_min_version | \
         awk 'BEGIN { FS = "."; } \
	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
       if test $rcudb_vers -ge $rcudb_regu ; then 
            rcudb_found=yes
       fi
    fi
    AC_MSG_RESULT($rcudb_found - is $rcudb_version) 

    # Some autoheader templates. 
    AH_TEMPLATE(HAVE_RCUDB, [Whether we have rcudb])

    if test "x$rcudb_found" = "xyes" ; then
        # Now do a check whether we can use the found code. 
        save_LDFLAGS=$LDFLAGS
	save_CPPFLAGS=$CPPFLAGS
    	LDFLAGS="$LDFLAGS -L$RCUDB_LIBDIR $RCUDB_LIBS"
    	CPPFLAGS="$CPPFLAGS $RCUDB_CPPFLAGS"
 
        # Change the language 
        AC_LANG_PUSH(C++)

	# Check for a header 
        have_rcudb_server_h=0
        AC_CHECK_HEADER([rcudb/Server.h], 
	                [have_rcudb_server_h=1])

        # Check the library. 
        have_librcudb=no
        AC_MSG_CHECKING(for -lrcudb)
        AC_LINK_IFELSE([
        AC_LANG_PROGRAM([#include <rcudb/Sql.h>],
                        [new RcuDb::Sql("");])], 
                        [have_librcudb=yes])
        AC_MSG_RESULT($have_librcudb)

        if test $have_rcudb_server_h -gt 0    && \
            test "x$have_librcudb"   = "xyes" ; then

            # Define some macros
            AC_DEFINE(HAVE_RCUDB)
        else 
            rcudb_found=no
        fi
        # Change the language 
        AC_LANG_POP(C++)
    fi

    AC_MSG_CHECKING(where the RcuDb documentation is installed)
    if test "x$rcudb_url" = "x" && \
	test ! "x$RCUDB_PREFIX" = "x" ; then 
       RCUDB_URL=${RCUDB_PREFIX}/share/doc/rcudb/html
    else 
	RCUDB_URL=$rcudb_url
    fi	
    AC_MSG_RESULT($RCUDB_URL)
   
    if test "x$rcudb_found" = "xyes" ; then 
        ifelse([$2], , :, [$2])
    else 
        ifelse([$3], , :, [$3])
    fi
    AC_SUBST(RCUDB_URL)
    AC_SUBST(RCUDB_PREFIX)
    AC_SUBST(RCUDB_CPPFLAGS)
    AC_SUBST(RCUDB_INCLUDEDIR)
    AC_SUBST(RCUDB_LDFLAGS)
    AC_SUBST(RCUDB_LIBDIR)
    AC_SUBST(RCUDB_LIBS)
    AC_SUBST(RCUDB_LTLIBS)
    AC_SUBST(RCUDB_LTLDFLAGS)
])
