dnl ------------------------------------------------------------------
dnl
dnl Check for Mysql
dnl AC_MYSQL([MIN_VERSION=5.0.0
dnl            [, ACTION-IF_FOUND 
dnl              [, ACTION-IF-NOT-FOUND]]])
dnl
AC_DEFUN([AC_MYSQL],
[
  AC_ARG_WITH([mysql-prefix],
	      [AC_HELP_STRING([--with-mysql-prefix=DIR],
		[Specify Mysql client installation prefix])], 
	      [with_mysql_prefix=$withval],[with_mysql_prefix=none])

  if test ! x"$with_mysql_prefix" = xnone; then
    mysql_bin="$with_mysql_prefix/bin"
  else 
    mysql_bin=$PATH
  fi
  AC_PATH_PROG(MYSQL_CONF, mysql_config, no, $mysql_bin)
	
  have_mysql=no
  if test ! x"$MYSQL_CONF" = "xno" ; then 
    # define some variables 
    changequote(<<, >>)dnl
    MYSQL_CFLAGS=`$MYSQL_CONF --cflags | sed 's/-[ID][^ ]*//g'` 
    MYSQL_CPPFLAGS=`$MYSQL_CONF --cflags | sed 's/\(-[ID][^ ]*\)/\1/g'` 
    MYSQL_LDFLAGS=`$MYSQL_CONF --libs | sed 's/-l[^ ]*//g'`
    MYSQL_LIBS=`$MYSQL_CONF --libs | sed 's/\(-l[^ ]*\)/\1/g'`
    MYSQL_VERS=`$MYSQL_CONF --version` 
    MYSQL_PORT=`$MYSQL_CONF --port` 
    changequote([, ])dnl

    # Check the version number is OK.
    mysql_min_version=ifelse([$1], ,5.0.0,$1)
    AC_MSG_CHECKING(if MySQL client library version is >= $mysql_min_version)
    mysql_vers=`echo $MYSQL_VERS | \
      awk 'BEGIN { FS = "."; } \
    	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
    mysql_regu=`echo $mysql_min_version | \
      awk 'BEGIN { FS = "."; } \
    	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
    if test $mysql_vers -ge $mysql_regu ; then 
         have_mysql=yes
    fi
    AC_MSG_RESULT([$have_mysql - $MYSQL_VERS])
  fi
  if test "x$have_mysql" = "xyes" ; then 
    ifelse([$2], , :, [$2])
  else 
    ifelse([$3], , :, [$3])
  fi
  AC_SUBST(MYSQL_CFLAGS)
  AC_SUBST(MYSQL_LDFLAGS)
  AC_SUBST(MYSQL_CPPFLAGS)
  AC_SUBST(MYSQL_LIBS)
  AC_SUBST(MYSQL_VERS)
  AC_SUBST(MYSQL_PORT)
])		

#
# EOF
#
