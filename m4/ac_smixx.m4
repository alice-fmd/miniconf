dnl ------------------------------------------------------------------
dnl
dnl
dnl
AC_DEFUN([AC_SMIXX],[
  AC_ARG_WITH([smixx],
	      [AC_HELP_STRING([--with-smixx=PREFIX],
	                      [Prefix of SMI installation])],
	      smixx_prefix=$withval, smixx_prefix=)
  have_smixx=no
  if test "x$smixx_prefix" != "xno" ; then 
    smixx_path=$PATH
    if test "x$smixx_prefix" != "x" ; then 
      smixx_path="${smixx_prefix}/bin:$PATH"
    fi
    AC_PATH_PROG(SMIXX_CONFIG,smixx-config,,${smixx_path})
    
    if test "x$SMIXX_CONFIG" = "x" ; then 
      have_smixx=no
    else
      have_smixx=yes
      smixx_reguired=ifelse([$1], , :, [$1])  
      changequote(<<, >>)dnl
      SMIXX_VERSION=`$SMIXX_CONFIG --version | sed 's/.*\([0-9][0-9]\.[0-9][0-9]\)/\1/'`
      smixx_vcode=`echo $SMIXX_VERSION  | awk 'BEGIN{FS="."}{printf "%d", $1*100+$2}'`
      smixx_rcode=`echo $smixx_requried | awk 'BEGIN{FS="."}{printf "%d", $1*100+$2}'`
      changequote([, ])dnl
      AC_MSG_CHECKING(whether SMI version is >= $smixx_required)
      if test $smixx_vcode -lt $smixx_rcode ; then 
        have_smixx=no
      fi
      AC_MSG_RESULT(${have_smixx}, is $smixx_version)
    fi
    
    if test "x$have_smixx" = "xyes" ; then 
      changequote(<<, >>)dnl
      SMIXX_LIBDIR=`$SMIXX_CONFIG --libdir`
      SMIXX_INCDIR=`$SMIXX_CONFIG --incdir`
      SMIXX_CPPFLAGS=`$SMIXX_CONFIG --cppflags`
      SMIXX_LDFLAGS=`$SMIXX_CONFIG --ldflags | sed 's/-l\([^ ][^ ]*\)//g'`
      SMIXX_LIBS=`$SMIXX_CONFIG --libs | sed 's/-[^l]\([^ ][^ ]*\)//g'`
      changequote([, ])dnl

      AC_LANG_PUSH([C++])
      save_CPPFLAGS=$CPPFLAGS
      save_LDFLAGS=$LDFLAGS
 
      CPPFLAGS="$CPPFLAGS $SMIXX_CPPFLAGS"
      LDFLAGS="$LDFLAGS $SMIXX_LDFLAGS"
      AC_CHECK_HEADERS([smirtl.hxx],[have_smixx=no],
	               

      CPPFLAGS=$save_CPPFLAGS
      LDFLAGS=$save_LDFLAGS
      AC_LANG_POP([C++])
    fi
])

dnl
dnl EOF
dnl 
